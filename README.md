# Parent Theme

WP parent theme code base that will be used to abstract child website themes.  This theme will be a core dependency for all WP theme development, but will not be an theme in itself that can/should be activated on a WP site.