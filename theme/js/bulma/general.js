var general;

jQuery(document).ready(function($) {
  
  general = {

    init: function() {

      this.dismissals();

    },

    dismissals: () => {

      $("body").on('click', ".dismissable .delete,.dismissable .close,.dismissable .dismiss", function(e) {

        var parent  = $(e.target).parents(".dismissable");
        var id      = $(parent).attr("id");
        var rem     = $(parent).data("remember");
        
        if( rem === true ) {

          general.cookie.set(id, "dismissed", { path: "/" });
          general.remove(parent);

        } else {

          general.remove(parent);

        }
      });

    },

    remove: (el) => {

      $(el)
        .addClass("transition")
        .on(
          "transitionend MSTransitionEnd webkitTransitionEnd oTransitionEnd",
          function() {
            $(el).remove();
          }
        ).css(theme.styles.remove(el));

    },

    dispatchEvent: function( name, data ) {

      data.type = name;
      $.event.trigger(data);

      if(theme.isLoggedIn())
        console.log( name, data );

    },

    cookie: {

      set:  (name, val, opts) => {

        var newCookie = encodeURIComponent(name) + "=" + encodeURIComponent(val) + ";";

        if(typeof opts == "object") {
          for(o in opts) {
            newCookie += o;
            if(opts[o] && opts[o] != "") {
              newCookie += "=";
              newCookie += opts[o] + ";";
            } else {
              newCookie += ";";
            }
          }
        }

        document.cookie = newCookie;

      },

      get: (name) => {

        var val =  false;
        var cookies = decodeURIComponent(document.cookie).split(";");

        cookies.forEach(function(obj, i) {
          if( ! val ) {
            if(obj.search(name) != -1) {
              val = obj.split("=")[1];
            }
          }
        });

        return val;

      },

      clear: (name) => {
        general.cookie.set( name, "", { expires: "Thu, 01 Jan 1970 00:00:01 UTC", path: "/" } );
      }

    }

  }


  try {
    general.init();
  } catch( ex ) {
    console.log("Err caught:", ex);
  }

});
