
var nav;

jQuery(document).ready(function($) {

  nav = {

    posChanged: false,

    init: function() {

      this.hamburger();
      
      if( typeof theme.header.position != "undefined" && false !== theme.header.position.changeOn )
        this.position();

    },

    hamburger: () => {

      $("body").on("click", ".navbar-burger", function(e) {
        $("html").toggleClass("nav-open");
      });

      $("body").on("click", "a.navbar-item, article, main, footer", function(e) {
        if( $(e.currentTarget).attr("href") === "#" || effects.resize.isSize("desktop"))
          return;

        $("html").removeClass("nav-open");
      });

    },

    position: function() {

      var threshold = theme.header.position.changeOn;
      var screen    = effects.resize.isSize({});

      if( threshold.indexOf( screen ) !== -1 )
        this.updateClasses();

      $(window).on( "viewportChange", function(e) {

        if( threshold.indexOf(e.screen) >= 0 && false === nav.posChanged )
          nav.updateClasses();
        else if( threshold.indexOf(e.screen) < 0 && true === nav.posChanged )
          nav.updateClasses();


      } );

    },

    updateClasses: () => {

      $("body").toggleClass(theme.header.position.bodyToggleClass);
      $(theme.header.id).toggleClass(theme.header.position.toggleClass);

      if( nav.posChanged )
        nav.posChanged = false;
      else
        nav.posChanged = true; 

    }

  };

  try {
    nav.init();
  } catch (ex) {
    console.log("Error caught: ", ex);
  }

});
