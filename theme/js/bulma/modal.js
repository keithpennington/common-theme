
var modal;

jQuery(document).ready(function($) {

  modal = {

    animationDelay: 500,
    activeBodyClass: "modal-shown",
    origEvent: null,
    type: null,

    basic: {
      activeClass: "is-active",
      selector: "#basic-modal",
      content: {
        activeClass: "shown",
        isVisible: false,
        selector: ".modal-content",
        target: null,
        parent: {
          element: null,
          contentIndex: null
        }
      }
    },

    image: {
      activeClass: "is-active",
      selector: "#image-modal",
      content: {
        activeClass: "shown",
        isVisible: false,
        selector: ".modal-content .image",
        target: null,
        parent: {
          element: null,
          contentIndex: null
        }
      }
    },

    card: {
      activeClass: "is-active",
      selector: "#card-modal",
      content: {
        activeClass: "shown",
        isVisible: false,
        selector: ".modal-card-body",
        target: null,
        parent: {
          element: null,
          contentIndex: null
        }
      },
      title: {
        selector: ".modal-card-title",
        value: null
      }
    },

    triggers: {
      open: '[data-toggle="modal"]',
      close: '.modal .close,.modal-close,.modal-background,.modal-card-foot button,.modal-card .delete',
      load: '[data-auto-trigger="true"]'
    },

    init: function(params) {

      if( Object.keys(params).length > 0 ) {

        var title = "undefined" == typeof params.title ? null : params.title;

        this.props.store(params.type, params.content, title, {});
        this.open();

      } else {

        this.setupTriggers();

      }

    },

    setupTriggers: function() {

      var self = this;

      $(document).on('click', self.triggers.open, function(e) {

        e.preventDefault();

        var target = $(e.currentTarget).attr("href");
        var type = $(e.currentTarget).data("type");
        var title = $(e.currentTarget).data("title");

        self.props.store(type, $(target), title, e);
        self.open();
        self.keyPressClose(true);

      });

      $(document).on('click', self.triggers.close, function(e) {

        self.origEvent = e;
        self.close();
        self.keyPressClose(false);

      });

    },

    dispatchEvent: function(name) {

      var currentModal = modal[modal.type];
      var data = {
        clickEvent: modal.origEvent,
        modal: currentModal,
        target: currentModal.content.target
      };

      // Global project event dispatcher for
      // public JS events that front-end devs
      // can subcribe to for various reasons
      general.dispatchEvent( name, data );

    },

    open: function() {

      var self = this;

      this.dispatchEvent("bulma.modal.show");

      this.showModal(function() {

        self.dispatchEvent("bulma.modal.shown");

      });

    },

    close: function() {

      var self = this;

      this.dispatchEvent("bulma.modal.hide");
      
      this.hideModal(this.type, function() {

        self.dispatchEvent("bulma.modal.hidden");
        try {

          self.props.reset();
        } catch(ex) {
          console.log(ex);
        }

      });
      
    },

    keyPressClose: function(init) {

      var self = this;

      function keyclose(e) {

        var pressed = e.key || e.keyCode;
        if( pressed === "Escape" || pressed === 27 ) {

          self.origEvent = e;
          self.close();

        }

      }

      if( init )
        $(document).on('keyup', keyclose);
      else
        $(document).off('keyup', keyclose);

    },

    showModal: function(callback) {

      var m = this[this.type];

      $(m.selector).addClass(m.activeClass);

      setTimeout(function() {

        if( "undefined" != typeof m.title && null !== m.title.value )
          $(m.selector).find(m.title.selector).html(m.title.value);

        $(m.selector)
          .find(m.content.selector)
          .append($(m.content.target).show());

        $("html").addClass(modal.activeBodyClass);

        if(callback)
          callback();

      }, 20);

    },

    hideModal: function(type, callback) {

      var self = this;
      var m = self[type];

      $("html").removeClass(modal.activeBodyClass);

      setTimeout(function() {

        $(m.selector).removeClass(m.activeClass);

        if(callback)
          callback();

      }, self.animationDelay);
    },

    props: {

      // Set properties to remember where
      // the target content should be restored
      // when the modal is closed
      store: function(type, content, title, e) {

        var m = modal[type];
        // If the node has a parent, store info about it
        if( $(content).parent().length > 0 ) {

          if($(content).is(":visible"))
            m.content.isVisible = true;

          m.content.parent.element = $(content).parent();
          m.content.parent.contentIndex = $(m.content.parent.element).children().index(content);

        }

        if( null !== title && "undefined" != typeof m.title )
          m.title.value = title;

        modal.type = type;
        modal.origEvent = e;
        m.content.target = content;

      },

      // Reset the stored properties
      // to ensure artifacts don't conflict
      // with future modal openings
      reset: function() {

        var m = modal[modal.type];

        if( null != m.content.parent.element ) {

          if( m.content.parent.contentIndex > 0 ) {

            var i = m.content.parent.contentIndex - 1;
            var sibling = $(m.content.parent.element).children().eq(i);

            if( ! m.content.isVisible)
              $(m.content.target).hide();

            $(m.content.target).insertAfter(sibling);

          } else {

            $(m.content.target).appendTo(m.content.parent);

          }

          if( "undefined" != typeof m.title ) {
            $(m.selector).find(m.title.selector).html("");
            m.title.value = null;
          }

          m.content.isVisible = false;
          m.content.parent.element = null;
          m.content.parent.contentIndex = null;

        } else {

          $(m.content.target).remove();

        }

        modal.type = null;
        m.content.target = null;

      }
    }

  };

  try {
    modal.init({});
  } catch (ex) {
    console.log("Error caught: ", ex);
  }

});
