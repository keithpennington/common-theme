
var tooltip;

jQuery(document).ready(function($) {

  tooltip = {

    init: function() {

      var self = this;
      var els = $("[data-tooltip]");

      if( null == els || els.length === 0)
        return false;

      els.forEach(function(obj, _i) {

        var tooltip = $(obj).data('tooltip');

        if($(obj).data('trigger')) {

          var trigger = $(obj).data("trigger");
          var pos = $(obj).data("position");

          self[trigger](obj,tooltip, self.styles(pos));
        }
      });
    },

    hover: function(el, tooltip, styles) {

      var self = this;
      var base = self.baseEl('hover', tooltip);

      $(el).on('mouseover', function(e) {

        console.log('Tooltip mouseover.');
        $(base)
          .css(styles)
          .appendTo('body');
      });

      $(el).on('mouseout', function(e) {

        console.log('Tooltip mouseout.');
        $(base).remove();
      });
    },

    click: function(el, tooltip, styles) {

      console.log("Tooltip click.");

    },

    load: function(el, tooltip, styles) {

      console.log('Tooltip load.');
      // var self = this;
      // var base = self.baseEl('load', tooltip);


    },

    styles: function(el, pos) {
      var offset = $(el).offset()[pos];
      var styles = {

      };

      switch(pos) {

        case "top":
          break;
        case "right":
          break;
        case "left":
          break;
        case "bottom":
          break;
      }

      return styles;
    },

    baseEl: function(trigger, tooltip) {

      return $(`<span class='tooltip ${trigger}'>${tooltip}</span>`);
    }

  };

  try {
    tooltip.init();
  } catch (ex) {
    console.log("Error caught: ", ex);
  }

});
