
jQuery(document).ready(function($) {

  window.maps = {

    maps: [],
    markers: [],
    script: null,
    styles: [],

    init: function() {

      this.callback();
      this.setStyles();
      this.enqueue();

    },

    callback:  () => {

      window.renderMaps = maps.render;

    },

    setStyles: () => {

      window.maps.styles = mapStyles;

    },

    enqueue:function() {

      var src             = "https://maps.googleapis.com/maps/api/js?key={key}&callback=renderMaps";
      this.script         = document.createElement('script');
      this.script.defer   = true;
      this.script.src     = src.replace("{key}", loadVars.mapKey);
      document.head.appendChild(this.script);

    },

    render: () => {

      $( ".map" ).each( (i, el) => {

        var optionKey = $(el).data("map");

        if( typeof loadVars.mapOptions[optionKey] !== 'undefined' ) {

          var options           = loadVars.mapOptions[optionKey];
          options.styles        = maps.styles;
          maps.maps[optionKey]  = new google.maps.Map(el, options);

          loadVars.mapMarkers[optionKey].forEach( (opts, n) => {

            opts.map = maps.maps[optionKey];
            maps.markers[optionKey] = new google.maps.Marker( opts );

          } );

        }

      } );

    },

  };

  window.maps.init();

});
