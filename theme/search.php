<?php

/**
 * Search template file used to render search queries
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

if( $wp_query->is_search() ) {

  // Utility::debug([$wp_query, $terms]);
  $search = get_search_query();

}

$sidebar    = apply_filters(
  'sidebar',
  false,
  [
    'location'  => "search",
    'object'    => $search
  ]
);
$postTypes  = Theme\Parent\Defaults::instance()->getPostTypes();
$activeTab  = get_query_var( "type", "page" );

get_header();

Utility::getScopedTemplatePart(
  "template-parts/hero/hero",
  "search",
  [
    'title'       => "Search Results",
    'subtitle'    => "Showing results for \"{$search}\"",
    'post_types'  => $postTypes,
    'active_tab'  => $activeTab
  ]
) ?>

<main role="main">
  <section class="section">
    <div class="container content">
      <h2>Under Construction... </h2>
    </div>
  </section>
</main>

<?php

if( false !== $sidebar ) {

  Utility::getScopedTemplatePart(
    "template-parts/aside/aside",
    null,
    [
      'sidebar' => $sidebar
    ]
  );

}

get_footer();
