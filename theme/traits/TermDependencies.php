<?php

namespace Theme\Parent\Traits;

use Theme\Parent\Utility;

trait TermDependencies {
  
  public $activeHooks   = false;
  public $dependencies  = [];
  public $validTaxonomies;

  public function setDependency( $taxonomy, $action = "addremove" ) {

    switch( $action ) {

      case "add":
        $this->dependencies['add'][]    = $taxonomy;
        break;

      case "remove":
        $this->dependencies['remove'][] = $taxonomy;
        break;

      case "addremove":
        $this->dependencies['add'][]    = $taxonomy;
        $this->dependencies['remove'][] = $taxonomy;
        break;

      default:
        return;

    }

    if( false === $this->activeHooks )
      $this->handleDependencies();

  }

  public function handleDependencies() {

    if( ! empty( $this->dependencies ) ) {

      add_action( 'saved_term', [ $this, "addCorrespondingTerm" ], 10, 4 );
      add_action( 'delete_term', [ $this, "removeCorrespondingTerm" ], 10, 5 );

      $this->validTaxonomies  = array_keys( $this->taxonomies );
      $this->activeHooks      = true;

    } else {

      remove_action( 'saved_term', [ $this, "addCorrespondingTerm" ] );
      remove_action( 'delete_term', [ $this, "removeCorrespondingTerm" ] );

    }

  }

  public function addCorrespondingTerm( $term_id, $tt_id, $taxonomy, $update ) {

    if( true === $update || ! isset( $this->dependencies['add'] ) || empty( $this->dependencies['add'] ) || ! in_array( $taxonomy, $this->validTaxonomies ) )
      return;

    $current = get_term( $term_id );

    foreach( $this->dependencies['add'] as $dependentTax ) {

      $dependents = get_terms( [
        'taxonomy' => $dependentTax,
        'hide_empty' => false
      ] );

      $dependentSlugs = array_column( $dependents, "slug" );

      if( ! in_array( $current->slug, $dependentSlugs ) ) {
        wp_insert_term(
          $current->name,
          $dependentTax,
          [
            'slug'        => $current->slug,
            'description' => sprintf("%s automatically added by the %s %s.", rtrim( ucfirst( $dependentTax ), "s" ), $current->name, rtrim( $taxonomy, "s" ) )
          ]
        );
      }

    }

  }

  public function removeCorrespondingTerm( $term_id, $tt_id, $taxonomy, $deleted_term, $object_ids ) {

    if( ! isset( $this->dependencies['remove'] ) || empty( $this->dependencies['remove'] ) || $taxonomy !== $this->name )
      return;

    foreach( $this->dependencies['remove'] as $dependentTax ) {

      $dependent = get_term_by( "slug", $deleted_term->slug, $dependentTax );

      if( false !== $dependent )
        wp_delete_term( $dependent->term_id, $dependentTax );

    }

  }

}
