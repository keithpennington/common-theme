<?php

namespace Theme\Parent\Traits;

use Theme\Parent\Utility;

trait Redirect {

  private $_traitRewrites;

  public function redirects( $rewrites ) {

    $this->_traitRewrites = $rewrites;
    add_action( 'parse_request', [ $this, "sitemapTemplates" ] );

  }

  public function sitemapTemplates( $wp ) {

    foreach( $this->_traitRewrites as $rule => $template ) {

      if( preg_match( $rule, $wp->request, $matches ) ) {

        Utility::getScopedTemplatePart(
          $template,
          null,
          [ 'params' => $matches ]
        );

        exit;

      }

    }

  }
  
}
