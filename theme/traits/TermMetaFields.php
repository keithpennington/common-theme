<?php

namespace Theme\Parent\Traits;

use Theme\Parent\{ Field, Utility };

trait TermMetaFields {

  private function _getTaxMetabox( $name ) {

    $fields = [];

    if( isset( $this->{$name . "Metabox"} ) )
      $fields = $this->{$name . "Metabox"};
    elseif( isset( $this->taxonomies[$name]['metabox'] ) )
      $fields = $this->taxonomies[$name]['metabox'];

    return $fields;

  }

  protected function termMetaFields( $taxonomy ) {

    add_action( "{$taxonomy}_edit_form", [ $this, "renderMetaFields"], 10, 2 );
    add_action( "saved_{$taxonomy}", [ $this, "saveTaxonomy" ], 10, 3 );

  }
  
  public function saveTaxonomy( $term_id, $tt_id, $update ) {

    $term     = get_term( $term_id );
    $box      = $this->_getTaxMetabox( $term->taxonomy );

    if( ! isset( $_POST[$box['nonce']['name']] ) || ! wp_verify_nonce( $_POST[$box['nonce']['name']], $box['nonce']['action'] ) )
      return;

    $options  = isset( $_POST[$box['id']] ) ? $_POST[$box['id']] : [];
    $values   = Field::checkboxValues( $options, $box['fields'] );

    update_term_meta( $term_id, "_" . $box['id'], $values );

  }

  public function renderMetaFields( $term, $taxonomy ) {
    
    $box          = $this->_getTaxMetabox( $taxonomy );
    $box['term']  = $term;

    Utility::getScopedTemplatePart(
      "template-parts/Bootstrap/form/form",
      "term-metabox",
      $box
    );

  }

}
