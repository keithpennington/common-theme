<?php

namespace Theme\Parent\Traits;

use Theme\Parent\{ Field, Utility };

trait PostMetaFields {
  use AddRenderMetabox, SaveCustomFields;
}

trait AddRenderMetabox {

  public function getPostTypes() {

    $types = false;

    if( property_exists( $this, "postTypes" ) )
      $types = $this->postTypes;
    elseif( property_exists( $this, "slug" ) )
      $types = [ $this->slug ];

    return $types;

  }

  public function getMetaboxes() {
    
    if( isset( $this->metaboxes ) && ! is_null( $this->metaboxes ) )
      $metaboxes = $this->metaboxes;
    else
      $metaboxes = [];

    return $metaboxes;

  }

  public function addMetabox( $post_types, $box ) {

    add_meta_box(
      $box['id'],
      $box['title'],
      [ $this, 'renderMetabox' ],
      $post_types,
      $box['context'],
      $box['priority'],
      [
        'fields'  => $box['fields'],
        'nonce'   => $box['nonce']
      ]
    );

  }
  
  public function renderMetabox( $post, $metabox ) {

    wp_nonce_field( $metabox['args']['nonce']['action'], $metabox['args']['nonce']['name'] );

    $metaFields = apply_filters( 'metabox_fields', $metabox['args']['fields'], $post );
    $metaFields = apply_filters( "{$metabox['id']}_fields", $metaFields, $post );
    $values     = get_post_meta( $post->ID, '_' . $metabox['id'], true );

    foreach( $metaFields as $field ) {

      $fieldId      = sprintf( "%s[%s]", $metabox['id'], $field['id'] );
      $value        = "";

      if( isset( $values[$field['id']] ) )
        $value = $values[$field['id']];
      elseif( isset( $field['options']['default'] ) )
        $value = $field['options']['default'];

      $renderField  = new Field( $field['type'], $fieldId, $value, $field['options'] );

      if( $renderField )
        $renderField->markup();

    }

  }

}

trait SaveCustomFields {

  public function saveCustomFields( $post_id, $post, $update ) {

    $post       = get_post( $post_id );
    $postTypes  = $this->getPostTypes();
    $metaboxes  = $this->getMetaboxes();

    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
      return;

    if( empty( $metaboxes ) )
      return;

    if( ! in_array( $post->post_type, $postTypes ) && ! array_key_exists( $post->post_type, $postTypes ) )
      return;

    foreach( $metaboxes as $metabox ) {

      $nonce = isset( $_POST[$metabox['nonce']['name']] ) ? wp_verify_nonce( $_POST[$metabox['nonce']['name']], $metabox['nonce']['action'] ) : false;

      if( ! $nonce )
        continue;

      $post   = isset( $_POST[$metabox['id']] ) ? $_POST[$metabox['id']] : '';
      $values = Field::checkboxValues( $post, $metabox['fields'] );
      
      update_post_meta( $post_id, '_' . $metabox['id'], $values );

    }

  }

}
