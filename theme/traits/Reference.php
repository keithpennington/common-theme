<?php

namespace Theme\Parent\Traits;

trait Reference {

  /**
   * Group
   * A slug used as the key to group similar items.
   * possible values: 'shortcodes', 'postTypes', 'menus'
   * Passed to the _themeReference method from the object's construct.
   * 
   * @var (string)
   */
  private $_referenceGroup;

  /**
   * Theme Reference
   * Checks that a printReference method is available and hooks this object's
   * reference materials for printing to the Theme Reference page.
   * 
   * @return void
   */
  private function _themeReference( $group ) {

    if( method_exists( $this, "printReference" ) ) {

      $this->_referenceGroup = $group;
      add_filter( 'theme_reference_items', [ $this, "addToReference" ], 10, 2 );

    }

  }
  /**
   * Add to Reference
   * Filter callback for adding the details about this object for
   * printing to the Theme Reference page.
   * 
   * @param $items (array) the included dynamic items in the Theme Reference
   * @see /parent.theme/template-parts/Bootstrap/admin/theme-reference.php
   * 
   * @return $items (array)
   */
  public function addToReference( array $items ): array {

    $items[$this->_referenceGroup][] = [
      'method'  => get_class( $this ) . "::printReference",
      'slug'    => $this->slug,
      'name'    => $this->name
    ];

    return $items;

  }

}
