<?php

namespace Theme\Parent;

include( TEMPLATEPATH . "/traits/Instance.php" );

class Loader {

  use Traits\Instance;

  protected $_ThemeSupport;
  protected $_Menus;
  protected $_PostTypes;
  protected $_Shortcodes;
  protected $_SettingsPages;
  protected $_Widgets;

  protected $_sidebars;

  protected $_scripts;
  protected $_styles;
  protected $_stylesheetFramework;
  protected $_frontPageMetabox;

  public $imageSizes = [
    [
      'crop'  => 0,
      'slug'  => "mobile",
      'name'  => "Mobile",
      'sizes' => [
        320,
        320
      ]
    ],
    [
      'crop'  => 0,
      'name'  => "Large Mobile",
      'slug'  => "large-mobile",
      'sizes' => [
        480,
        480
      ]
    ],
    [
      'crop'  => 0,
      'name'  => "Tablet",
      'slug'  => "tablet",
      'sizes' => [
        768,
        768
      ]
    ],
    [
      'crop'  => 0,
      'name'  => "Desktop",
      'slug'  => "desktop",
      'sizes' => [
        1024,
        1024
      ]
    ],
    [
      'crop'  => 0,
      'name'  => "High Resolution",
      'slug'  => "hi-res",
      'sizes' => [
        2048,
        2048
      ]
    ]
  ];
  public $features;
  public $postTypes = [];

  public static $availableFrameworks = [
    "Bootstrap",
    "Bulma",
    "Vanilla"
  ];

  function __construct() {

    self::$_instance = $this;

    if( method_exists( $this, "_stylesheetSetup" ) )
      $this->_stylesheetSetup();

    $this->_templateSetup();

    // Actions
    add_action( 'after_setup_theme', [ $this, "themeSupport" ], 3 );
    add_action( 'after_setup_theme', [ $this, "createImageSizes" ], 4 );
    add_action( 'init', [ $this, "registerEntities" ], 1 );
    add_action( 'widgets_init', [ $this, "registerSidebars" ] );
    add_action( 'widgets_init', [ $this, "themeWidgets" ], 55 );

    foreach( [ "wp_enqueue_scripts", "admin_enqueue_scripts" ] as $hook ) {

      add_action( $hook, [ $this, "scripts" ] );
      add_action( $hook, [ $this, "styles" ] );
      add_action( $hook, [ $this, "localizedJS" ] );

    }

    add_action( 'wp_footer', [ $this, "siteSchema" ], 2 );
    add_action( 'wp_footer', [ $this, "noScripts" ] );

    // Filters
    if( $this->_frontPageMetabox && ! empty( $this->_frontPageMetabox ) )
      add_filter( 'default_theme_metaboxes', [ $this, "frontPageMetabox"] );

    add_filter( 'auto_update_plugin', '__return_false' );
    add_filter( 'get_site_icon_url', [ $this, "favicon" ], 10, 3 );
    add_filter( 'wp_title', [ $this, "wpTitle" ], 10, 3 );
    add_filter( 'intermediate_image_sizes', [ $this, "reduceImageSizes" ] );
    add_filter( 'image_size_names_choose', [ $this, "imageChoices" ] );
    add_filter( 'language_attributes', [ $this, "ogHtmlAttr" ], 10, 2 );
    add_filter( 'wp_get_attachment_image_attributes', [ $this, "imageAtts" ], 20, 3 );
    add_filter( 'wp_sitemaps_enabled', [ $this, "disableDefaultSitemap" ] );
    add_filter( 'theme_templates', [ $this, "reduceTemplates" ], 10, 4 );
    add_filter( 'wp_mail_from', [ $this, "mailFrom" ] );
    add_filter( 'wp_mail_from_name', [ $this, "mailFromName" ] );
    add_filter( 'upload_mimes', [ $this, "addMimeTypes" ], 4 ) ;

    if( defined( "PROD_URL" ) ) {

      add_filter( 'wp_get_attachment_image_src', [ $this, "resetImageBase" ], 10, 4 );
      add_filter( 'upload_dir', [ $this, "resetUploadDir" ] );

    }

  }

  private function _templateSetup() {

    $inClasses = [
      "Utility",
      "Defaults",
      "Field",
      "Menu",
      "PostType",
      "Shortcode",
      "Sitemap",
      "SettingsPage",
      "Users"
    ];

    $inTraits = [
      "PostMetaFields",
      "Redirect",
      "Reference",
      "TermDependencies",
      "TermMetaFields"
    ];

    foreach( $inTraits as $inT )
      $this->_include( TEMPLATEPATH . "/traits/", $inT );

    foreach( $inClasses as $inC )
      $this->_include( TEMPLATEPATH . "/classes/", $inC );

    if( is_admin() || ! defined( "CONTENTFRAMEWORK" ) )
      define( "CONTENTFRAMEWORK", "Bootstrap" );

    if( isset( $this->_stylesheetFramework ) && ! empty( $this->_stylesheetFramework ) )
      $this->_include( TEMPLATEPATH . "/classes/Frameworks/", $this->_stylesheetFramework );

    $this->_handleFeatures();

    add_editor_style( STYLESHEETPATH . "/css/theme.min.css" );

  }

  private function _handleFeatures() {

    if( ! isset( $this->features['wpemoji'] ) ) {

      remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
      remove_action( 'wp_print_styles', 'print_emoji_styles' );

      remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
      remove_action( 'admin_print_styles', 'print_emoji_styles' );

    }

  }

  private function _includeFromThemePaths( $object, $folder, $construct = true ) {

    $objects = "_" . $object . "s";

    if( ! empty( $this->$objects ) ) {

      foreach( $this->$objects as $name ) {

        if( $this->_include( STYLESHEETPATH . $folder, $name ) ) {

          $class = "Theme\Child\\" . $object . "\\" . $name;

          if( $construct && class_exists( $class ) ) {

            $instance = new $class();
            Utility::objectUpdates( $instance );

          }
          else
            error_log( "Missing class " . $class );

        } elseif( $this->_include( TEMPLATEPATH . $folder, $name ) ) {

          $class = "Theme\Parent\\" . $object . "\\" . $name;

          if( $construct && class_exists( $class ) ) {

            $instance = new $class();
            Utility::objectUpdates( $instance );
          }
          else
            error_log( "Missing class " . $class );

        }

      }

    }

  }

  protected function _include( $path, $name ) {

    $status = false;
    $file = $path . $name . ".php";

    if( file_exists( $file ) ) {

      include_once( $file );
      $status = true;

    }

    return $status;

  }

  public function themeSupport() {

    if( is_array( $this->_ThemeSupport ) && ! empty( $this->_ThemeSupport ) ) {

      foreach( $this->_ThemeSupport as $feature ) {
        if( isset( $feature['args'] ) )
          add_theme_support( $feature['name'], $feature['args'] );
        else
          add_theme_support( $feature['name'] );

      }

    }

  }

  public function createImageSizes() {

    foreach( $this->imageSizes as $imgSize ) {

      add_image_size( $imgSize['slug'], $imgSize['sizes'][0], $imgSize['sizes'][1], $imgSize['crop'] );

    }

  }

  public function registerEntities() {

    $this->_includeFromThemePaths( "PostType", "/classes/PostTypes/" );
    $this->_includeFromThemePaths( "Shortcode", "/classes/Shortcodes/" );
    $this->_includeFromThemePaths( "SettingsPage", "/classes/SettingsPages/" );
    $this->_includeFromThemePaths( "Menu", "/classes/Menus/" );

    // Build the sitemap after all objects are registered.
    new Sitemap();

  }

  public function registerSidebars() {

    if( is_array( $this->_sidebars ) && ! empty( $this->_sidebars ) ) {

    foreach( $this->_sidebars as $s )
      register_sidebar($s);

    }

  }

  public function themeWidgets() {

    global $wp_registered_widgets;

    if( is_array( $this->_Widgets ) && ! empty( $this->_Widgets ) ) {

      foreach( $this->_Widgets['deactivate'] as $widget )
          unregister_widget( $widget );

      foreach( $this->_Widgets['activate'] as $widget ) {
        // TO DO:  Add registration process for widgets.
      }


    }

  }

  public function scripts() {

    if( is_array( $this->_scripts ) && ! empty( $this->_scripts ) ) {

      $scripts = true === is_admin() ? $this->_scripts['admin'] : $this->_scripts['front'];

      if( is_array( $scripts ) && ! empty( $scripts ) ) {

        foreach( $scripts as $s ) {

          if( ! isset( $s['dir'] ) )
            continue;

          $dirFunc = "get_" . $s['dir'] . "_directory_uri";

          if( ! function_exists( $dirFunc ) )
            continue;

          $deps = isset( $s['dependencies'] ) ? $s['dependencies'] : [];
          $ver  = isset( $s['ver'] ) ? $s['ver'] : false;

          if( array_key_exists( 'name', $s) && array_key_exists( 'file', $s ) )
            wp_enqueue_script( $s['name'], $dirFunc() . $s['file'], $deps, $s['ver'], $ver);

        }

      }

    }

  }

  public function styles() {

    if( is_array( $this->_styles ) && ! empty( $this->_styles ) ) {

      $styles = true === is_admin() ? $this->_styles['admin'] : $this->_styles['front'];

      if( is_array( $styles ) && ! empty( $styles ) ) {

        foreach( $styles as $s ) {

          if( ! isset( $s['dir'] ) )
            continue;

          $dirFunc  = "get_" . $s['dir'] . "_directory_uri";

          if( ! function_exists( $dirFunc ) )
            continue;

          $deps = isset( $s['dependencies'] ) ? $s['dependencies'] : [];
          $ver  = isset( $s['ver'] ) ? $s['ver'] : false;

          if( array_key_exists( 'name', $s ) && array_key_exists( 'file', $s ) )
            wp_enqueue_style( $s['name'], $dirFunc() . $s['file'], $deps, $ver );

        }

      }

    }

  }

  public function localizedJS() {

    if( ! is_admin() ) {
      
      $l10n = [
        'hostname'  => get_option( 'siteurl' ),
        'uaIds'     => array_map( 'trim', explode( ",", Utility::getOption( 'google_props', 'ga_id' ) ) ),
        'gtmIds'    => array_map( 'trim', explode( ",", Utility::getOption( 'google_props', 'gtm_id' ) ) ),
        'mapKey'    => Utility::getOption( 'google_props', 'maps_api_key' )
      ];

      $code = "var loadVars = " . json_encode( apply_filters( 'localized_js', $l10n ) ) . ";";
      wp_add_inline_script( "theme", $code, "before" );

    }

  }

  public function favicon( $url, $size, $blog_id ) {

    $url = get_stylesheet_directory_uri() . "/assets/favicon.png";
    return $url;

  }

  public function siteSchema() {

    $schema = json_encode( Utility::getSchema() );

    echo sprintf( "\n<script type=\"application/ld+json\">\n%s\n</script>\n", $schema );

  }

  public function noScripts() {

    $gtmContainers = array_map( 'trim', explode( ",", Utility::getOption( 'google_props', 'gtm_id' ) ) );

    foreach( $gtmContainers as $container )
      echo sprintf( "<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=%s\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n", $container );

  }

  public function frontPageMetabox( $metaboxes ) {

    if( is_admin() && isset( $_GET['post'] ) && isset( $_GET['action'] ) ) {
      
      if( "edit" === $_GET['action'] && $_GET['post'] === get_option( 'page_on_front' ) )
        $metaboxes[] = $this->_frontPageMetabox;

    }

    return $metaboxes;

  }

  public function wpTitle( $title, $sep, $sepLocation ) {

    if( is_front_page() )
      $title = get_the_title() . " $sep " . get_bloginfo( 'blogname' );
    else
      $title = $title . get_bloginfo( 'blogname' );

    return $title;

  }

  public function reduceImageSizes( $defaults ) {

    $themeSizes = array_column( $this->imageSizes, "slug" );
    $keepSizes  = array_merge( [ "thumbnail" ], $themeSizes );

    return array_intersect( $defaults, $keepSizes);

  }

  public function imageChoices( $choices ) {

    $themeSizes = array_combine(
      array_column( $this->imageSizes, "slug" ),
      array_column( $this->imageSizes, "name" )
    );

    $choices = array_merge( $choices, $themeSizes );

    return $choices;

  }

  public function ogHtmlAttr( $output, $doctype ) {

    return $output . ' prefix="og: https://ogp.me/website#"';

  }

  public function imageAtts( $attr, $attachment, $size ) {

    $dimensions   = wp_get_attachment_image_src( $attachment->ID );
    $orientation  = "landscape";

    if( $dimensions[2] > $dimensions[1] )
      $orientation = "portrait";

    $attr['class'] .= " {$orientation}-oriented";

    return $attr;

  }

  public function disableDefaultSitemap( $enabled ) { return false; }

  public function reduceTemplates( $templates, $wpTheme, $post, $postType ) {

    foreach( $templates as $name => $info ) {

      if( ! stristr( $name, $this->_stylesheetFramework ) )
        unset( $templates[$name] );

    }

    return $templates;

  }

  public function mailFrom( $email ) { return "wordpress@lightsoncreative.com"; }

  public function mailFromName( $name ) { return "Lights On Creative [WordPress]"; }

  public function resetImageBase( $image, $attach_id, $size, $icon ) {

    $image = str_replace( WP_SITEURL, PROD_URL, $image );

    return $image;

  }

  public function resetUploadDir( $dir ) {

    $dir['baseurl'] = str_replace( WP_SITEURL, PROD_URL, $dir['baseurl'] );
    $dir['url']     = str_replace( WP_SITEURL, PROD_URL, $dir['url'] );

    return $dir;

  }

  public function addMimeTypes( $types ) {

    if ( current_user_can( 'upload_files' ) ) {

      $types['svg']   =  "image/svg+xml";
      $types['webp']  =  "image/webp";

    }

    return $types;

  }

}
