<?php
/**
 * The template for displaying the footer
 *
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

$defaultClasses = [
  "footer",
  "hero"
];

ob_start() ?>

    </article>
    <footer id="site-footer">

      <?php do_action( 'pre_footer' ) ?>

      <section class="<?php echo implode( " ", apply_filters( 'site_footer_classes', $defaultClasses ) ) ?>">
        <div class="content has-text-centered">
          <?php

            echo apply_filters(
              'footer_copyright',
              sprintf(
                "<p>&copy; %s <a target='_new' href='https://simard.solutions'>Lights On Creative</a></p>",
                date( 'Y' )
              )
            );

          ?>
        </div>
      </section>

      <?php do_action( 'post_footer') ?>
      
    </footer>

    <?php

    Utility::getScopedTemplatePart(
      "template-parts/Vanilla/ui/ui",
      "btt"
    );

    Utility::getScopedTemplatePart(
      "template-parts/ui/ui",
      "modal"
    );

    echo "<div id='message-board'>";
      echo "<div class='messages'>";
        do_action( 'message_board' );
      echo "</div>";
    echo "</div>";

    wp_footer() ?>

  </body>
</html>

<?php echo ob_get_clean();
