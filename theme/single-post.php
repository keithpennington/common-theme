<?php
/**
 * Post Single
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

get_header();

if ( have_posts() ) {

  while ( have_posts() ) {

    the_post();

    Utility::getScopedTemplatePart(
      "template-parts/content/content",
      "featured-image",
      [
        'sidebar'         => "blog"
      ]
    );

    Utility::getScopedTemplatePart(
      "template-parts/aside/aside",
      null,
      [
        'sidebar'         => "blog"
      ]
    );

  }

} else {

  Utility::getScopedTemplatePart("template-parts/content/content", "none");
  
}

get_footer();
