<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

$sidebar = is_active_sidebar( "blog" ) ? "blog" : false;
$pageForPosts = get_option( 'page_for_posts' );

get_header();

Utility::getScopedTemplatePart(
  "template-parts/hero/hero",
  null,
  [
    'title' => get_the_title($pageForPosts)
  ]
);

Utility::getScopedTemplatePart(
  "template-parts/content/content",
  "feed",
  [
    'sidebar' => $sidebar
  ]
);

if( false !== $sidebar ) {

  Utility::getScopedTemplatePart(
    "template-parts/aside/aside",
    null,
    [
      'sidebar' => $sidebar
    ]
  );

}

get_footer();
