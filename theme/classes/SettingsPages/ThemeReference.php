<?php

namespace Theme\Parent\SettingsPage;

use Theme\Parent\{ SettingsPage as Page, Utility };

class ThemeReference extends Page {
  
  protected $_args = [
    'id'          => 'reference',
    'menu_title'  => 'Theme Reference',
    'page_title'  => "Theme Reference",
    'menu_type'   => 'Menu',
    'icon'        => "dashicons-location-alt",
    'position'    => 59
  ];

  public function pageRenderCallback() {

    Utility::getScopedTemplatePart( "template-parts/admin/theme", "reference", $this->_args );

  }

}
