<?php

namespace Theme\Parent;

class Defaults {

  use Traits\PostMetaFields, Traits\Instance;

  public $metaboxes = [
    [
      'id'        => "seo_props",
      'context'   => "advanced",
      'title'     => "SEO Settings",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "sitemap_exclusion",
          'type'    => "checkbox",
          'options' => [
            'label'     => "Exclude from Sitemap",
            'desc'      => "Checking this box will prevent search engines from reading and indexing this page."
          ]
        ],
        [
          'id'      => "og_title",
          'type'    => "input",
          'options' => [
            'label'   => "Open Graph Title",
            'desc'    => "Write a custom open graph title that will be different from this post's title and used when third parties embed a preview of this page. If left blank, the default page title will be used.",
            'render'  => '<input id="%s" name="%s" type="text" class="form-control" value="%s" />'
          ]
        ],
        [
          'id'      => "meta_title",
          'type'    => "input",
          'options' => [
            'label'   => "Meta Title",
            'desc'    => "Write a custom meta title that will be different from this post's title and used when bots crawl this site. If left blank, the default page title will be used.",
            'render'  => '<input id="%s" name="%s" type="text" class="form-control" value="%s" />'
          ]
        ]
      ],
      'nonce' => [
        'name'    => "loc_theme_seo_properties",
        'action'  => "loc_theme_save_seo_properties"
      ]
    ]
  ];

  public $postTypes = [
    'post' => "Post",
    'page' => "Page"
  ];

  function __construct() {

    self::$_instance = $this;

    // Actions
    add_action( 'init', [ $this, "postTypeSupport" ] );
    add_action( 'add_meta_boxes', [ $this, "addMetaboxes" ] );
    add_action( 'save_post', [ $this, "saveCustomFields" ], 10, 3 );
    add_action( 'save_post', [ $this, "overrideAuthor" ], 10, 3 );
    add_action( 'wp_footer', [ $this, "bannerDisplay" ], 10 );


    // Filters
    add_filter( 'render_block', [ $this, "defaultBlockRenderFixes" ], 10, 2 );

  }

  public function postTypeSupport() {

    add_post_type_support( "page", "excerpt" );

  }

  public function getPostTypes() {

    return apply_filters( 'default_meta_post_types', $this->postTypes );

  }

  public function getMetaboxes() {

    return apply_filters( 'default_theme_metaboxes', $this->metaboxes );

  }

  public function addMetaboxes() {

    $metaboxes = $this->getMetaboxes();

    if( is_array( $metaboxes ) && ! empty( $metaboxes ) ) {

      foreach( $metaboxes as $box )
        $this->addMetabox( array_keys( $this->getPostTypes() ), $box );

    }

  }

  public function overrideAuthor( $postID, $post, $update ) {

    $override = Utility::getOption( "business_info", "override_author" );
    $owner    = Utility::getOption( "business_info", "owner" );

    if( "revision" !== $post->post_type && in_array( $override, Utility::trueTerms() ) && $owner !== $post->post_author )
      wp_update_post( [ 'ID' => $postID, 'post_author' => $owner ] );

  }

  public function defaultBlockRenderFixes( $content, $block ) {

    // Remove empty paragrph tags from rendering if they have no contents
    if( isset( $block['blockName'] ) && "core/paragraph" == $block['blockName'] && preg_match( "/<p>[\w\W]{0}<\/p>/", $block['innerHTML'] ) )
      $content = "";

    // Restore missing alt text from images rendered in cover block
    if( isset( $block['blockName'] ) && "core/cover" == $block['blockName'] && preg_match( "/alt=\"\"/", $block['innerHTML'] ) )
      $content = $this->_fixMissingAltAttributes( $content, $block );

    return $content;

  }

  private function _fixMissingAltAttributes( $content, $block ) {
    
    $alt      = get_post_meta( $block['attrs']['id'], '_wp_attachment_image_alt', true );
    $pattern  = "/(wp-image-{$block['attrs']['id']}[^>]+) (alt=\"\")/";
    $replace  = sprintf( "$1 alt=\"%s\"", esc_attr( $alt ) );

    $content = preg_replace( $pattern, $replace, $content );

    return $content;

  }

  public function bannerDisplay() {

    $args = [
      'show'            => Utility::getOption( 'banner_settings', 'show' ),
      'message'         => Utility::getOption( 'banner_settings', 'message' ),
      'modal_content'   => Utility::getOption( 'banner_settings', 'modal_content' ),
      'modal_toggle'    => Utility::getOption( 'banner_settings', 'modal_toggle' )
    ];

    if( in_array( $args['show'], Utility::trueTerms() ) )
      Utility::getScopedTemplatePart( 'template-parts/Vanilla/content/content', 'banner', $args );

  }

  
}

new Defaults();
