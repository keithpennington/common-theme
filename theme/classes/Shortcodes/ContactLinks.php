<?php

namespace Theme\Parent\Shortcode;

use Theme\Parent\{ Shortcode, Utility };

class ContactLinks extends Shortcode {
  
  use \Theme\Parent\Traits\Instance;

  public $slug = "contact_links";
  public $name = "Contact Links";
  
  protected static $_instance;

  public function execute( $atts, $content = null ) {

    $atts = shortcode_atts( [
      'align'         => "center",
      'format'        => "icons-only",  // icon-list, list-only
      'orientation'   => "",    // vertical
      'show'          => "phone, email, facebook, instagram"
    ], $atts, $this->slug );

    $linkAtts = [
      'phone' => [
        'id'      => "click-to-call",
        'class'   => "btn btn-tertiary",
        'href'    => Utility::getPhone( true ),
        'target'  => "_self",
        'icon'    => "fa fa-phone",
        'label'   => Utility::getPhone()
      ],
      'email' => [
        'id'      => "click-to-mail",
        'class'   => "btn btn-secondary",
        'href'    => Utility::getEmail( true ),
        'target'  => "_self",
        'icon'    => "fa fa-envelope",
        'label'   => Utility::getEmail()
      ],
      'facebook' => [
        'id'      => "fb-follow",
        'class'   => 'btn btn-primary',
        'href'    => Utility::getFacebookUrl(),
        'target'  => "_blank",
        'icon'    => "fab fa-lg fa-facebook",
        'label'   => Utility::getFacebookUrl( true )
      ],
      'instagram' => [
        'id'      => "insta-follow",
        'class'   => "btn btn-primary",
        'href'    => Utility::getInstagramUrl(),
        'target'  => "_blank",
        'icon'    => "fab fa-lg fa-instagram",
        'label'   => Utility::getInstagramUrl( true )
      ]
    ];

    $links = apply_filters( 'contact_link_atts', $linkAtts );
    $items = array_map( "trim", explode( ",", $atts['show'] ) );
    $output = "";

    $ul = "<ul class='%s'>%s</ul>";
    $li = "<li><a id='%s' target='%s' class='%s' href='%s'>%s</a></li>";
    $span = "<span>%s</span>";
    $icon = "<i class='%s'></i>";

    foreach( $items as $item ) {

      if( isset( $links[$item] ) ) {

        $fa = sprintf( $icon, $links[$item]['icon'] );

        if( "icons-only" !== $atts['format'] )
          $label = sprintf( $span, $links[$item]['label'] );

        if( "list-only" !== $atts['format'] )
          $class = $links[$item]['class'];

        $output .= sprintf(
          $li,
          $links[$item]['id'],
          $links[$item]['target'],
          $class,
          $links[$item]['href'],
          $fa . $label
        );

      }
    
    }

    $containerClass = implode( " ", [ "contact-block", $atts['align'], $atts['format'], $atts['orientation'] ] );
    $output = sprintf(
      $ul,
      $containerClass,
      $output
    );

    return apply_filters( 'contact_links', $output, $links );

  }

  public static function printReference() {

    ob_start() ?>

    <strong>Basic Usage:</strong> <code>[contact_links]</code>

    <p class="card-text mt-2">
      Shortcode to render the stored methods of contact from the <a href="/wp-admin/admin.php?page=business">Business Settings</a>.
    </p>
    <table class="table mt-2">
      <thead class="thead-light">
        <tr>
          <th scope="col">Attribute</th>
          <th scope="col">Functionality</th>
          <th scope="col">Default</th>
          <th scope="col">Possible Values</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">
            align
          </th>
          <td>
            Text alignment to use for the contact links.
          </td>
          <td>
            "center"
          </td>
          <td>
            "left", "center", "right"
          </td>
        </tr>
        <tr>
          <th scope="row">
            format
          </th>
          <td>
            A choice of styling for showing the contact links.
          </td>
          <td>
            "icons-only"
          </td>
          <td>
            One of "icon-list", "icons-only" or "list-only". Icon list render stylized buttons with icons and labels in a stack, based on the text alignment.  Icons only shows stylized buttons with only icons.  List only shows unstyled links with icons.
          </td>
        </tr>
        <tr>
          <th scope="row">
            orientation
          </th>
          <td>
            Change the orientation for just the "icons-only" format.
          </td>
          <td>
            "horizontal"
          </td>
          <td>
            "vertical"
          </td>
        </tr>
        <tr>
          <th scope="row">
            show
          </th>
          <td>
            Choose which contact links to show.  They will render in the order listed.
          </td>
          <td>
            "phone, email, facebook, instagram" <i>(all)</i>
          </td>
          <td>
            "phone", "email", "facebook", "instagram", or a comma-separated list of any combination of them.
          </td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Example</th>
          <th scope="col">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">
            1
          </td>
          <td>
            <code>[contact_links format="icons-only"]</code>
          </td>
          <td>
            This is how the contact links currently render on the homepage.
          </td>
        </tr>
        <tr>
          <th scope="row">
            2
          </td>
          <td>
            <code>[contact_links format="list-only" show="phone, email"]</code>
          </td>
          <td>
            Show a hyperlinked list of just the phone and email contacts.
          </td>
        </tr>
        <tr>
          <th scope="row">
            3
          </td>
          <td>
            <code>[contact_links format="icon-list" align="right"]</code>
          </td>
          <td>
            Show stylized buttons with icons and labels, flushed right.
          </td>
        </tr>
      </tbody>
    </table>

    <?php echo ob_get_clean();

  }

}
