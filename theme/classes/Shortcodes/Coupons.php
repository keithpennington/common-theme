<?php

namespace Theme\Parent\Shortcode;

use Theme\Parent\{ Shortcode, Utility };
use WP_Query;

class Coupons extends Shortcode {
  
  use \Theme\Parent\Traits\Instance;

  public $name = "Coupons";
  public $slug = "coupons";
  
  protected static $_instance;

  public function execute( $atts, $content = null ) {

    $atts = shortcode_atts( [
      'layout'    => "card",
      'promo'     => "",
      'category'  => "",
      'limit'     => -1
    ], $atts, $this->slug );

    $args = [
      'post_type'       => "coupon",
      'posts_per_page'  => floatval( $atts['limit'] ),
      'orderby'         => "menu_order",
      'order'           => "ASC"
    ];

    if( ! empty( $atts['category'] ) ) {

      $terms              = array_map( "trim", explode( ",", $atts['category'] ) );
      $args['tax_query']  = [
        'relationship' => "OR",
        [
          'taxonomy'  => "category",
          'field'     => "slug",
          'terms'     => $terms
        ],
        [
          'taxonomy'  => "category",
          'field'     => "name",
          'terms'     => $terms
        ]
      ];

    }

    $coupons = new WP_Query( $args );

    ob_start();

    if( $coupons->have_posts() ) {

      echo sprintf( "<div class=\"columns coupon-layout-%s\">\n", $atts['layout'] );

      while( $coupons->have_posts() ) {

        $coupons->the_post();

        if( ! empty( $atts['promo'] ) ) {

          $thisType = Utility::getPostMetaValue( "details", "promo" );
          $allowedTypes = array_map( "trim", explode( ",", $atts['promo'] ) );

          if( ! in_array( $thisType, $allowedTypes ) )
            continue;

        }

        echo "<div class='column'>";

        Utility::getScopedTemplatePart(
          "template-parts/layout/coupon",
          $atts['layout']
        );

        echo "</div>";

      }

      echo "</div>";

      wp_reset_postdata();

    } elseif( ! is_null( $content ) ) {

      echo do_shortcode( $content );

    } else {

      echo apply_filters( 'no_coupons', "No coupons found." );

    }

    $output = ob_get_clean();
    return apply_filters( "html_{$this->slug}_output", $output, $atts, $coupons );

  }

  public static function printReference() {

    ob_start() ?>

    <strong>Basic Usage:</strong> <code>[coupons]</code>
    <p class="card-text mt-2">
      Shortcode to print coupons on a page.  Attributes allow customizing which coupons should show, and how many.  Coupons are always ordered by the "Order" setting found in the Coupon Attributes box in the edit screen.  These can also be set in the <a target="_new" href="/wp-admin/edit.php?post_type=coupon">Coupon List</a> page by using the "Quick Edit" link.
    </p>
    <table class="table mt-2">
      <thead class="thead-light">
        <tr>
          <th scope="col">Attribute</th>
          <th scope="col">Functionality</th>
          <th scope="col">Default</th>
          <th scope="col">Possible Values</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">
            layout
          </th>
          <td>
            Change the layout template for the coupons.
          </td>
          <td>
            "card"
          </td>
          <td>
            "card" only currently.
          </td>
        </tr>
        <tr>
          <th scope="row">
            promo
          </th>
          <td>
            Only show coupons of a certain promo type. These are set in the Coupon Details section of the edit screen on each coupon. These may be usable for integrated ad syndication later.
          </td>
          <td>
            <var>all</var>
          </td>
          <td>
            "bogo", "discount", "free", "price"
          </td>
        </tr>
        <tr>
          <th scope="row">
            category
          </th>
          <td>
            A comma-separated list of the Coupon Category names or slugs that you want to limit your coupons by. This is a taxonomy that can be applied to coupons in the edit screen.
          </td>
          <td>
            <var>all</var>
          </td>
          <td>
            Any user-added categories on the site that are applied to coupons.
          </td>
        </tr>
        <tr>
          <th scope="row">
            limit
          </th>
          <td>
            Define how many coupons you want to display. Must be an integer.
          </td>
          <td>
            <var>all</var>
          </td>
          <td>
            "3", "10", "20", etc.
          </td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Example</th>
          <th scope="col">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">
            1
          </td>
          <td>
            <code>[coupons category="Lawn Care, Landscaping"]</code>
          </td>
          <td>
            Show all coupons that are in the "Lawn Care" or "Landscaping" categories.
          </td>
        </tr>
        <tr>
          <th scope="row">
            2
          </td>
          <td>
            <code>[coupons limit="3" promo="free"]</code>
          </td>
          <td>
            Show 3 coupons of a "Free" promo type.
          </td>
        </tr>
        <tr>
          <th scope="row">
            3
          </td>
          <td>
            <code>[coupons promo="bogo" category="Rug Cleaning"]</code>
          </td>
          <td>
            Show all coupons with a Rug Cleaning category, that are listed as a "bogo" promotion type.
          </td>
        </tr>
      </tbody>
    </table>

    <?php echo ob_get_clean();

  }

}
