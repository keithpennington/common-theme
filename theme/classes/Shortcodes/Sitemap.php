<?php

namespace Theme\Parent\Shortcode;

use Theme\Parent\{ Shortcode, Utility, Sitemap\Walker as SitemapWalker };
use WP_Query;

class Sitemap extends Shortcode {
  
  use \Theme\Parent\Traits\Instance;

  public $slug = "sitemap";
  public $name = "Sitemap";
  
  protected static $_instance;

  public function execute( $atts, $content = null ) {

    $contentTypes = [
      [
        'name'        => "post",
        'title'       => "Blog Posts",
        'changefreq'  => "weekly",
        'priority'    => "0.3"
      ],
      [
        'name'        => "page",
        'title'       => "Main Pages",
        'changefreq'  => "monthly",
        "priority"    => "0.5"
      ]
    ];
    $contentTypes = apply_filters( 'sitemap_content_types', $contentTypes );

    $args = apply_filters(
      'default_sitemap_args',
      [
        'post_status' => "publish",
        'nopaging'    => true,
        'orderby'     => "title",
        'order'       => "ASC"
      ],
      $contentTypes
    );

    ob_start();

    echo '<div class="columns is-multiline is-max-tablet">';

    if( ! empty( $contentTypes ) ) {

      foreach( $contentTypes as $type ) {

        if( taxonomy_exists( $type['name'] ) ) {

          $terms = get_terms( [
            'taxonomy'    => $type['name'],
            'hide_empty'  => "true"
          ] );

          echo "<div class='column is-half-tablet'>";
          echo "<h2>{$type['title']}</h2>";
          echo "<ul class='sitemap-list {$type['name']}-page-list'>";

          foreach( $terms as $term )
            echo sprintf( '<li><a href="%s">%s</a></li>', get_term_link( $term, $type['name'] ), $term->name );

          echo "</ul>";
          echo "</div>";

        } else {

          $args['post_type']  = $type['name'];
          $query              = new WP_Query( $args );

          if( $query->have_posts() ) {

            $walkerArgs = [
              'markup' => "html"
            ];
            echo "<div class='column is-half-tablet'>";
            echo "<h2>{$type['title']}</h2>";
            echo "<ul class='sitemap-list {$type['name']}-page-list'>";

            $sitemapWalker = new SitemapWalker();
            echo $sitemapWalker->walk( $query->posts, 0, $walkerArgs );

            echo "</ul>";
            echo "</div>";
            
          }

        }

      }

    }

    echo '</div>';

    $output = ob_get_clean();

    return apply_filters( 'shortcode_sitemap_output', $output, $args );

  }

}
