<?php

namespace Theme\Parent\Shortcode;

use Theme\Parent\{ Shortcode, Utility };

class Address extends Shortcode {
  
  use \Theme\Parent\Traits\Instance;

  public $slug = "address";
  public $name = "Address";

  protected static $_instance;

  public function execute( $atts, $content = null ) {

    $atts = shortcode_atts( [
      'parts'   => "all",
      'link'    => Utility::getMapsUrl(),
      'display' => "block",
      'icon'    => "fa-fw fa-lg fa-map-pin",
      'class'   => ""
    ], $atts, $this->slug );

    $fullAddress = Utility::getAddress();
    
    if( "all" !== $atts['parts'] ) {

      $props = array_map( "trim", explode( ",", $atts['parts'] ) );
      $props = array_intersect_key( $fullAddress, array_combine( $props, $props ) );

    } else {

      $props = $fullAddress;

    }

    $output = "";

    if( ! in_array( $atts['icon'], Utility::falseTerms() ) )
      $output .= sprintf( "<i class='fa %s'></i> ", $atts['icon'] );

    if( "block" == $atts['display'] ) {

      foreach( $props as $prop => $val )
        $output .= sprintf( "<span class='%s'>%s</span>" . ( "city" == $prop ? ', ' : ' ' ), $prop, $val );

    } else {

      $output .= sprintf( "<span>%s</span>", implode( " ", $props ) );

      if( -1 !== stripos( $output, $fullAddress['city'] . " " . $fullAddress['state'] ) )
        $output = str_replace( $fullAddress['city'], $fullAddress['city'] . ",", $output );

    }

    if( ! in_array( $atts['link'], Utility::falseTerms() ) )
      $output = sprintf( "<a aria-label='%s' class='%s' target='_new' href='%s'>%s</a>", __( "Get Directions", "site-theme" ), $atts['class'], $atts['link'], $output );

    if( "block" == $atts['display'] )
      $output = sprintf( "<div class='address block %s'>%s</div>", $atts['class'], $output );
    

    $address = apply_filters(
      'address',
      $output,
      $props
    );

    return $address;

  }

  public static function printReference() {

    ob_start() ?>

    <strong>Basic Usage:</strong> <code>[address]</code>

    <p class="card-text mt-2">
      Shortcode to render the business the address on a page.
    </p>
    <table class="table mt-2">
      <thead class="thead-light">
        <tr>
          <th scope="col">Attribute</th>
          <th scope="col">Functionality</th>
          <th scope="col">Default</th>
          <th scope="col">Possible Values</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">
            parts
          </th>
          <td>
            Print a single piece or specific pieces of the address, like just the street, or the city and state.
          </td>
          <td>
            all
          </td>
          <td>
            "street", "city", "state", "zip", or a comma-separated list of any of these.
          </td>
        </tr>
        <tr>
          <th scope="row">
            link
          </th>
          <td>
            Override, or disable the external map link on the address. Map links open in a new tab or window.
          </td>
          <td>
            A generated URL using the <a href="/wp-admin/admin.php?page=social">Google My Business Profile Shortname</a>, <a href="/wp-admin/admin.php?page=business">Map URL</a>, or <a href="/wp-admin/admin.php?page=business">Latitude / Longitude coordinates</a>&mdash;whichever is available, and found in this respective order.
          </td>
          <td>
            To override the default link, use any https:// URL. To disable the link, use a value of "disable", "off", "false", "hide", "no", "never", "nope" or "0".
          </td>
        </tr>
        <tr>
          <th scope="row">
            display
          </th>
          <td>
            Show the address as a block of text, or as inline to be used in content.
          </td>
          <td>
            block
          </td>
          <td>
            "block" or "inline"
          </td>
        </tr>
        <tr>
          <th scope="row">
            icon
          </th>
          <td>
            Set the icon shown before the map rendering.  This is a FontAwesome class.
          </td>
          <td>
            "fa-lg fa-map-pin"
          </td>
          <td>
            Any font awesome icon class. To disable the icon, use a value of "disable", "off", "false", "hide", "no", "never", "nope" or "0".
          </td>
        </tr>
      </tbody>
    </table>
    <table class="table">
      <thead class="thead-light">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Example</th>
          <th scope="col">Description</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">
            1
          </td>
          <td>
            <code>[address icon="fa-fw fa-map"]</code>
          </td>
          <td>
            Show a hyperlinked block display of the address with a custom icon.
          </td>
        </tr>
        <tr>
          <th scope="row">
            2
          </td>
          <td>
            <code>[address icon="hide" link="disable" display="inline" parts="street, city, state"]</code>
          </td>
          <td>
            Print only the street, city and state in inline text to be used seamlessly in  a paragraph &mdash; i.e. "<?php echo do_shortcode("[address icon=\"hide\" link=\"disable\" display=\"inline\" parts=\"street, city, state\"]") ?>"
          </td>
        </tr>
      </tbody>
    </table>

    <?php echo ob_get_clean();

  }

}
