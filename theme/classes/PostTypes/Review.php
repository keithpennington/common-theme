<?php

namespace Theme\Parent\PostType;

use Theme\Parent\{ PostType, Utility };

class Review extends PostType {

  use \Theme\Parent\Traits\TermMetaFields;
  use \Theme\Parent\Traits\Instance;

  public $name    = "Review";
  public $slug    = "review";
  public $updates = [
    '0.1' => "changeDetailKeys"
  ];
  public $args    = [
    'description'          => "Create reviews for use on the website.",
    'public'               => true,
    'hierarchical'         => false,
    'exclude_from_search'  => true,
    'publicly_queryable'   => false,
    'show_ui'              => true,
    'show_in_menu'         => true,
    'show_in_nav_menus'    => false,
    'show_in_admin_bar'    => true,
    'show_in_rest'         => true,
    'menu_position'        => 6,
    'menu_icon'            => "dashicons-format-status",
    'supports' => [
      "title",
      "page-attributes"
    ],
    'register_meta_box_cb' => [],
    'labels' => [
      'name'                       => "Reviews",
      'singular_name'              => "Review",
      'add_new'                    => "Add Review",
      'add_new_item'               => "Add New Review",
      'edit_item'                  => "Edit Review",
      'new_item'                   => "New Review",
      'view_item'                  => "View Review",
      'view_items'                 => "View Reviews",
      'search_items'               => "Search Reviews",
      'not_found'                  => "No Reviews found.",
      'not_found_in_trash'         => "No Reviews found in trash.",
      'all_items'                  => "All Reviews",
      'archives'                   => "Review Archives",
      'attributes'                 => "Review Attributes",
      'insert_into_item'           => "Insert into Review",
      'uploaded_to_this_item'      => "Uploaded to this Review",
      'featured_image'             => "Review Image",
      'set_featured_image'         => "Set Review image",
      'remove_featured_image'      => "Remove Review image",
      'use_featured_image'         => "Use as Review image",
      'menu_name'                  => "Reviews",
      'filter_items_list'          => "Filter Reviews list",
      'items_list_navigation'      => "Review list navigation",
      'items_list'                 => "Reviews list",
      'item_published'             => "Review published.",
      'item_published_privately'   => "Review published privately.",
      'item_reverted_to_draft'     => "Review reverted to draft.",
      'item_scheduled'             => "Review scheduled.",
      'item_updated'               => "Review updated."
    ]
  ];
  public $metaboxes = [
    [
      'id'        => "details",
      'context'   => "normal",
      'title'     => "Review Details",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "score",
          'type'    => "input",
          'options' => [
            'class'     => "review-score",
            'container' => "div",
            'label'     => "Score (1-5)",
            'render'    => '<input id="%s" name="%s" type="number" value="%s" placeholder="#" step=".1" min="1" max="5" />',
            'size'      => "small"
          ]
        ],
        [
          'id'      => "url",
          'type'    => "input",
          'options' => [
            'class'     => "is-half source-url",
            'container' => "div",
            'label'     => "Source URL",
            'render'    => '<input id="%s" name="%s" type="text" value="%s" placeholder="https://..." />',
            'size'      => "medium"
          ]
        ],
        [
          'id'      => "name",
          'type'    => "input",
          'options' => [
            'class'     => "reviewer-name",
            'container' => "div",
            'label'     => "Reviewer Name",
            'render'    => '<input id="%s" name="%s" type="text" value="%s" placeholder="First, Last" />'
          ]
        ],
        [
          'id'    => "profile_photo",
          'type'  => "input",
          'options' => [
            'class'     => "reviewer-icon",
            'container' => "div",
            'label'     => "Reviewer Icon",
            'render'    => '<input id="%s" name="%s" type="text" value="%s" placeholder="https://..." />'
          ]
        ],
        [
          'id'      => "location",
          'type'    => "input",
          'options' => [
            'class'     => "reviewer-location",
            'container' => "div",
            'label'     => "Reviewer Location",
            'render'    => '<input id="%s" name="%s" type="text" value="%s" placeholder="City, Region/Country" />'
          ]
        ],
        [
          'id'      => "description",
          'type'    => "input",
          'options' => [
            'class'     => "review-description",
            'container' => "div",
            'label'     => "Description",
            'render'    => '<textarea id="%s" name="%s" placeholder="Enter the full review..." rows="10" cols="50">%s</textarea>'
          ]
        ],
      ],
      'nonce' => [
        'name'    => "loc_review_details",
        'action'  => "save_loc_review_details"
      ]
    ]
  ];
  public $taxonomies = [
    'review_source' => [
      'show_ui'                => true,
      'show_admin_column'      => true,
      'show_in_nav_menus'      => false,
      'show_in_rest'           => true,
      'show_tagcloud'          => false,
      'query_var'              => true,
      'labels' => [
        'name'                    => "Sources",
        'singular_name'           => "Source",
        'search_items'            => "Search Sources",
        'all_items'               => "All Sources",
        'edit_item'               => "Edit Source",
        'update_item'             => "Update Source",
        'add_new_item'            => "Add New Source",
        'new_item_name'           => "New Source Name",
        'menu_name'               => "Sources",
        'not_found'               => "No Sources founds.",
        'no_terms'                => "No Sources",
        'items_list_navigation'   => "Sources list navigation",
        'items_list'              => "Sources list",
        'back_to_items'           => "&larr; Back to Sources"
      ],
      'metabox' => [
        'id'      => "details",
        'title'   => "Source Details",
        'fields'  => [
          [
            'id'      => "icon",
            'type'    => "group-text",
            'options' => [
              'desc'        => "Choose an icon class from <a target='_new' href='https://fontawesome.com/icons?d=gallery&m=free'>Font Awesome</a>.",
              'label'       => "Icon Class",
              'placeholder' => "e.g. fab fa-google",
              'size'        => "medium"
            ]
          ]
        ],
        'nonce'   => [
          'name'    => "loc_theme_review_source_meta",
          'action'  => "loc_theme_save_review_source_meta"
        ]
      ]
    ]
  ];
  public $useDefaultMeta = false;

  function __construct() {

    parent::__construct();
    $this->termMetaFields( "review_source" );

    // Filters
    add_filter( 'slider_slide_query', [ $this, "setReviewSlider" ], 10, 2 );
    add_filter( 'slider_options', [ $this, "reviewSliderDefaults" ], 10, 2 );
    if( ! is_admin() )
      add_filter( 'the_content', [ $this, "renderReview" ] );

  }

  public function setReviewSlider( $query, $atts ) {

    if( isset($atts['name'] ) && "reviews" === $atts['name'] ) {

      $query = [
        'post_type'   => "review",
        'post_status' => "publish",
        'orderby'     => "order",
        'order'       => "ASC"
      ];

      if( isset( $atts['source'] ) ) {

        $query['tax_query'][] = [
          'taxonomy'  => "source",
          'field'     => "slug",
          'value'     => $atts['source']
        ];

      }

      if( isset( $atts['limit'] ) )
        $query['limit'] = $atts['limit'];

    }

    return $query;

  }

  public function reviewSliderDefaults( $options, $atts ) {
    
    if( isset( $atts['name'] ) && "reviews" === $atts['name'] ) {

      $options['pageDots']        = true;
      $options['cellSelector']    = ".review-slide";
      $options['adaptiveHeight']  = true;

    }

    return $options;

  }

  public function renderReview( $content ) {

    global $post;

    if( "review" === $post->post_type ) {

      $reviewDetails  = get_post_meta( $post->ID, "_details", true );
      $terms          = wp_get_post_terms( $post->ID, "review_source" );
      $sourceDetails  = get_term_meta( $terms[0]->term_id, "_details", true );

      ob_start();

      Utility::getScopedTemplatePart(
        "template-parts/article/review",
        "box",
        [
          'review_details'  => $reviewDetails,
          'source_details'  => $sourceDetails,
          'source'          => $terms[0]
        ]
      );

      $content = ob_get_clean();

    }

    return $content;

  }

  public function changeDetailKeys() {

    $keyChanges = [
      'score'       => "reviewRating",
      'description' => "reviewBody"
    ];
    $reviews = get_posts( [
      'post_type'   => $this->name
    ] );

    if( $reviews && ! empty( $reviews ) ) {

      foreach( $reviews as $review ) {

        $details = get_post_meta( $review->ID, "_details", true );
        $targetVals = array_intersect_key( $details, $keyChanges );

        foreach( $targetVals as $oldKey => $value ) {

          $details[$keyChanges[$oldKey]] = $value;
          unset( $details[$oldKey] );

        }

        update_post_meta( $review->ID, "_details", $details );

      }

    }

  }

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      Reviews can be used to showcase customer accolades across the website.  The Review post has a taxonomy called <a href="/wp-admin/edit-tags.php?taxonomy=review_source&post_type=review">sources</a> which can be used to mark the origination of a review, and set an icon to represent that review source.
    </p>
    <div class="alert alert-warning">
      Currently the Reviews are not connected to any data source and need to be manually updated.  In the future, these will connect to business listing source like Google My Business and Yelp!
    </div>
    <a class="btn btn-info btn-sm" href="/wp-admin/edit.php?post_type=review">Edit Reviews</a>

    <?php echo ob_get_clean();

  }

}
