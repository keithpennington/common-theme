<?php

namespace Theme\Parent\PostType;

use Theme\Parent\{ PostType, Utility };

class Slide extends PostType {

  use \Theme\Parent\Traits\TermMetaFields;
  use \Theme\Parent\Traits\Instance;

  public $slug = "slide";
  public $name = "Slide";
  public $args = [
    'description'          => "Create slides for various media sliders that can be used across the website.",
    'public'               => true,
    'hierarchical'         => false,
    'exclude_from_search'  => true,
    'publicly_queryable'   => false,
    'show_ui'              => true,
    'show_in_menu'         => true,
    'show_in_nav_menus'    => false,
    'show_in_admin_bar'    => true,
    'show_in_rest'         => true,
    'menu_position'        => 11,
    'menu_icon'            => "dashicons-images-alt2",
    'supports' => [
      'custom-fields',
      'editor',
      'title',
      'page-attributes'
    ],
    'register_meta_box_cb' => [],
    'labels' => [
      'name'                       => "Slides",
      'singular_name'              => "Slide",
      'add_new'                    => "Add Slide",
      'add_new_item'               => "Add New Slide",
      'edit_item'                  => "Edit Slide",
      'new_item'                   => "New Slide",
      'view_item'                  => "View Slide",
      'view_items'                 => "View Slides",
      'search_items'               => "Search Slides",
      'not_found'                  => "No Slides found.",
      'not_found_in_trash'         => "No Slides found in trash.",
      'all_items'                  => "All Slides",
      'archives'                   => "Slide Archives",
      'attributes'                 => "Slide Attributes",
      'insert_into_item'           => "Insert into Slide",
      'uploaded_to_this_item'      => "Uploaded to this Slide",
      'featured_image'             => "Slide Image",
      'set_featured_image'         => "Set Slide image",
      'remove_featured_image'      => "Remove Slide image",
      'use_featured_image'         => "Use as Slide image",
      'menu_name'                  => "Slides",
      'filter_items_list'          => "Filter Slides list",
      'items_list_navigation'      => "Slide list navigation",
      'items_list'                 => "Slides list",
      'item_published'             => "Slide published.",
      'item_published_privately'   => "Slide published privately.",
      'item_reverted_to_draft'     => "Slide reverted to draft.",
      'item_scheduled'             => "Slide scheduled.",
      'item_updated'               => "Slide updated."
    ]
  ];
  public $taxonomies = [
    'sliders' => [
      'show_ui'                => true,
      'show_admin_column'      => true,
      'show_in_nav_menus'      => false,
      'show_in_rest'           => true,
      'show_tagcloud'          => false,
      'query_var'              => true,
      'rewrite'                => [ 'slug' => 'sliders' ],
      'labels' => [
        'name'                    => "Sliders",
        'singular_name'           => "Slider",
        'search_items'            => "Search Sliders",
        'all_items'               => "All Sliders",
        'edit_item'               => "Edit Slider",
        'update_item'             => "Update Slider",
        'add_new_item'            => "Add New Slider",
        'new_item_name'           => "New Slider Name",
        'menu_name'               => "Sliders",
        'not_found'               => "No Sliders founds.",
        'no_terms'                => "No Sliders",
        'items_list_navigation'   => "Sliders list navigation",
        'items_list'              => "Sliders list",
        'back_to_items'           => "&larr; Back to Sliders"
      ]
    ]
  ];
  public $metaboxes = [
    [
      'id'        => "options",
      'title'     => "Slide Options",
      'context'   => "side",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "link",
          'type'    => "input",
          'options' => [
            'label'     => "Slide Link",
            'desc'      => "Link this entire slide's contents to a page. Leave blank to disable.",
            'render'    => '<input type="text" id="%s" name="%s" value="%s" />'
          ]
        ],
        [
          'id'      => "target",
          'type'    => "select",
          'options' => [
            'label'     => "Link Target",
            'desc'      => "Choose the windox context for the link.",
            'options'   => [
              ''      => "Same window",
              '_new'  => "New Window"
            ]
          ],
          'default'     => ""
        ]
      ],
      'nonce'     => [
        'name'      => "loc_slide_options",
        'action'    => "loc_save_slide_options"
      ]
    ]
  ];
  public $slidersMetabox =[
    'id'      => "slider_settings",
    'title'   => "Slider Settings",
    'fields'  => [
      [
        'id'      => "autoPlay",
        'type'    => "group-input",
        'options' => [
          'append'      => "milliseconds",
          'desc'        => "Delay between slides. This is measured in milliseconds where 1000ms = 1s. So for 6.5s delay, use 6500.\n
            <br>Default: <code>Off</code> (leave blank)",
          'label'       => "Auto Play",
          'prepend'     => "Delay",
          'render'      => '<input type="number" id="%s" name="%s" value="%s" placeholder="Off" step="1" min="1000" %s/>',
          'size'        => "small"
        ],
        'default' => false
      ],
      [
        'id'      => "prevNextButtons",
        'type'    => "checkbox",
        'options' => [
          'desc'    => "Show the Next and Previous arrow buttons used to click through slides.\n
            <br>Default: <code>On</code>",
          'label'   => "Show Next/Previous Buttons"
        ],
        'default' => "on"
      ],
      [
        'id'      => "pageDots",
        'type'    => "checkbox",
        'options' => [
          'desc'    => "Show the small dots indicating the current slide position beneath the slider frame.\n
            <br>Default: <code>Off</code>\n",
          'label'   => "Show Index Dots"
        ],
        'default' => false
      ],
      [
        'id'      => "freeScroll",
        'type'    => "checkbox",
        'options' => [
          'desc'    => "Prevent the slider from snapping to slide frames.\n
            <br>Default: <code>Off</code>\n
            <span class='alert alert-light' style='display:block'><b class='fas fa-fw fa-lightbulb' style='color:var(--info)'></b> \n
              Enable this setting and compile a slideshow of a cropped panorama photo to make a draggable panorama slider.\n
            </span>",
          'label'   => "Free Scroll"
        ],
        'default' => false
      ],
      [
        'id'      => "wrapAround",
        'type'    => "checkbox",
        'options' => [
          'desc'    => "Slides rotate continuously from start to end.\n
            <br>Default: <code>Off</code>",
          'label'   => "Wrap Around"
        ],
        'default' => false
      ],
      [
        'id'      => "contain",
        'type'    => "checkbox",
        'options' => [
          'desc'    => "Remove slide offset.\n
            <br>Default: <code>Off</code>",
          'label'   => "Contain"
        ],
        'default' => false
      ],
      [
        'id'      => "adaptiveHeight",
        'type'    => "checkbox",
        'options' => [
          'desc'    => "Slider height adjusts to the active slide's unique content height.\n
            <br>Default: <code>Off</code>",
          'label'   => "Adaptive Height"
        ],
        'default' => false
      ],
      [
        'id'      => "slide-properties",
        'type'    => "section-break",
        'options' => [
          'title'   => "Slide Properties",
          'class'   => "settings-section-break"
        ],
        'default' => ""
      ],
      [
        'id'      => "slideWidth",
        'type'    => "group-input",
        'options' => [
          'class'       => "is-half",
          'desc'        => "Manually set the width of the slides. (e.g. '200px')<br />
          Acceptable units: <code>%</code>, <code>px</code>, <code>vw</code>, <code>em</code><br />
          Default: <code>90%</code>",
          'label'       => "Slide Width",
          'render'      => '<input type="text" id="%s" name="%s" value="%s" />',
          'size'        => "small"
        ],
        'default'   => "90%"
      ],
      [
        'id'      => "slidePadding",
        'type'    => "group-input",
        'options' => [
          'class'       => "is-half",
          'desc'        => "Manually set the padding of the slides. (e.g. '0em 1em')<br />
          Acceptable units: <code>%</code>, <code>px</code>, <code>vw</code>, <code>em</code><br />
          Default: <code>0px</code>",
          'label'       => "Slide Padding",
          'render'      => '<input type="text" id="%s" name="%s" value="%s" />',
          'size'        => "small"
        ],
        'default'   => "0px"
      ],
      [
        'id'      => "advanced-slide-settings",
        'type'    => "section-break",
        'options' => [
          'title'   => "Advanced Options",
          'class'   => "settings-section-break",
          'desc'    => "See <a target='_new' href='https://flickity.metafizzy.co/options.html#selectedattraction-friction'>flickity options</a> for full documentation."
        ],
        'default' => ""
      ],
      [
        'id'      => "selectedAttraction",
        'type'    => "group-text",
        'options' => [
          'desc'        => "Higher attraction makes the slider move faster. Lower makes it move slower.",
          'label'       => "Selected Attraction",
          'pattern'     => "[\.\d]*",
          'placeholder' => "0.025",
          'size'        => "small",
          'title'       => "Numbers and decimals only."
        ],
        'default' => 0.025
      ],
      [
        'id'      => "friction",
        'type'    => "group-text",
        'options' => [
          'desc'        => "Higher friction makes the slider feel stickier and less bouncy. Lower friction makes the slider feel looser and more wobbly.",
          'label'       => "Friction",
          'pattern'     => "[\.\d]*",
          'placeholder' => "0.28",
          'size'        => "small",
          'title'       => "Numbers and decimals only."
        ],
        'default' => 0.28
      ]
    ],
    'nonce'   => [
      'name'    => "nonce_slider_settings",
      'action'  => "saving_slider_settings"
    ]
  ];
  public $useDefaultMeta = false;

  public static $numberFields = [
    "autoPlay",
    "selectedAttraction",
    "friction"
  ];

  function __construct() {

    parent::__construct();

    $this->termMetaFields( "sliders" );

  }

  public static function sliderOptions(): array {

    $options = [
      '' => "Choose a slider"
    ];
    $sliders = get_terms([
      'taxonomy'    => 'sliders',
      'hide_empty'  => false
    ]);
    $values = array_column( $sliders, "slug");
    $labels = array_column( $sliders, "name");
    $options = $options + array_combine($values, $labels);

    return $options;

  }

}
