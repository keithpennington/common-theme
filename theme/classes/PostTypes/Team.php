<?php

namespace Theme\Parent\PostType;

use Theme\Parent\{ PostType, Utility };

class Team extends PostType {

  use \Theme\Parent\Traits\Instance;

  public $name = "Team Members";
  public $slug = "team_member";
  public $args = [
    'description'          => "Create team members and teams for dynamic syndication to staff pages.",
    'public'               => false,
    'hierarchical'         => false,
    'exclude_from_search'  => true,
    'publicly_queryable'   => false,
    'show_ui'              => true,
    'show_in_menu'         => true,
    'show_in_nav_menus'    => false,
    'show_in_admin_bar'    => true,
    'show_in_rest'         => true,
    'menu_position'        => 20,
    'menu_icon'            => "dashicons-groups",
    'supports' => [
      "editor",
      "page-attributes",
      "thumbnail",
      "title",
    ],
    'register_meta_box_cb' => [],
    'labels' => [
      'name'                       => "Team Members",
      'singular_name'              => "Team Member",
      'add_new'                    => "Add Team Member",
      'add_new_item'               => "Add New Team Member",
      'edit_item'                  => "Edit Team Member",
      'new_item'                   => "New Team Member",
      'view_item'                  => "View Team Member",
      'view_items'                 => "View Team Members",
      'search_items'               => "Search Team Members",
      'not_found'                  => "No Team Members found.",
      'not_found_in_trash'         => "No Team Members found in trash.",
      'all_items'                  => "All Team Members",
      'archives'                   => "Team Member Archives",
      'attributes'                 => "Team Member Attributes",
      'insert_into_item'           => "Insert into Team Member",
      'uploaded_to_this_item'      => "Uploaded to this Team Member",
      'featured_image'             => "Profile Image",
      'set_featured_image'         => "Set profile image",
      'remove_featured_image'      => "Remove profile image",
      'use_featured_image'         => "Use as profile image",
      'menu_name'                  => "Teams",
      'filter_items_list'          => "Filter Team Member list",
      'items_list_navigation'      => "Team Member list navigation",
      'items_list'                 => "Team Members list",
      'item_published'             => "Team Member published.",
      'item_published_privately'   => "Team Member published privately.",
      'item_reverted_to_draft'     => "Team Member reverted to draft.",
      'item_scheduled'             => "Team Member scheduled.",
      'item_updated'               => "Team Member updated."
    ]
  ];
  public $metaboxes = [
    [
      'id'        => "details",
      'context'   => "advanced",
      'title'     => "Team Member Details",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "title",
          'type'    => "input",
          'options' => [
            'label'     => "Job Title",
            'render'    => '<input type="text" id="%s" name="%s" value="%s" class="form-control" />'
          ]
        ],
        [
          'id'      => "linkedin",
          'type'    => "input",
          'options' => [
            'label'     => "LinkedIn Profile",
            'render'    => '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="https://linkedin.com/..." />'
          ]
        ],
        [
          'id'      => "facebook",
          'type'    => "input",
          'options' => [
            'label'     => "Facebook Profile",
            'render'    => '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="https://facebook.com/..." />'
          ]
        ],
        [
          'id'      => "twitter",
          'type'    => "input",
          'options' => [
            'label'     => "Twitter Profile",
            'render'    => '<input type="text" id="%s" name="%s" value="%s" class="form-control" placeholder="https://twitter.com/..." />'
          ]
        ]
      ],
      'nonce'     => [
        'name'    => "loc_team_member_details",
        'action'  => "save_loc_team_member_details"
      ]
    ]
  ];
  public $taxonomies = [
    'team' => [
      'hierarchical'        => true,
      'show_ui'             => true,
      'show_admin_column'   => true,
      'show_in_nav_menus'   => false,
      'show_in_rest'        => true,
      'show_tagcloud'       => false,
      'query_var'           => true,
      'labels'              => [
        'name'                    => "Teams",
        'singular_name'           => "Team",
        'search_items'            => "Search Teams",
        'all_items'               => "All Teams",
        'edit_item'               => "Edit Team",
        'update_item'             => "Update Team",
        'add_new_item'            => "Add New Team",
        'new_item_name'           => "New Team Name",
        'menu_name'               => "Teams",
        'not_found'               => "No Teams founds.",
        'no_terms'                => "No Teams",
        'items_list_navigation'   => "Team list navigation",
        'items_list'              => "Team list",
        'back_to_items'           => "&larr; Back to Teams"
      ]
    ]
  ];
  public $useDefaultMeta = false;

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      Team Members are a way to dynamically create a staff page with current employees by managing them in one place.
    </p>
    <p class="card-text">
      Team Members have a taxonomy of Teams that can be used to group them together for department-specific pages.
    </p>
    <a class="btn btn-info btn-sm" href="/wp-admin/edit.php?post_type=team">Edit Team Members</a>

    <?php echo ob_get_clean();

  }

}
