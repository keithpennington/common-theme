<?php

namespace Theme\Parent\PostType;

use Theme\Parent\{ PostType, Utility };

class Coupon extends PostType {

  use \Theme\Parent\Traits\TermMetaFields;
  use \Theme\Parent\Traits\Instance;

  public $name    = "Coupon";
  public $slug    = "coupon";

  public $args    = [
    'description'          => "Create Coupons for use on the website.",
    'public'               => true,
    'hierarchical'         => false,
    'exclude_from_search'  => true,
    'publicly_queryable'   => true,
    'show_ui'              => true,
    'show_in_menu'         => true,
    'show_in_nav_menus'    => false,
    'show_in_admin_bar'    => true,
    'show_in_rest'         => true,
    'menu_position'        => 23,
    'menu_icon'            => "dashicons-tickets-alt",
    'supports' => [
      "custom-fields",
      "editor",
      "page-attributes",
      "thumbnail",
      "title"
    ],
    'taxonomies'            => [
      "category"
    ],
    'register_meta_box_cb' => [],
    'labels' => [
      'name'                       => "Coupons",
      'singular_name'              => "Coupon",
      'add_new'                    => "Add Coupon",
      'add_new_item'               => "Add New Coupon",
      'edit_item'                  => "Edit Coupon",
      'new_item'                   => "New Coupon",
      'view_item'                  => "View Coupon",
      'view_items'                 => "View Coupons",
      'search_items'               => "Search Coupons",
      'not_found'                  => "No Coupons found.",
      'not_found_in_trash'         => "No Coupons found in trash.",
      'all_items'                  => "All Coupons",
      'archives'                   => "Coupon Archives",
      'attributes'                 => "Coupon Attributes",
      'insert_into_item'           => "Insert into Coupon",
      'uploaded_to_this_item'      => "Uploaded to this Coupon",
      'featured_image'             => "Coupon Image",
      'set_featured_image'         => "Set Coupon image",
      'remove_featured_image'      => "Remove Coupon image",
      'use_featured_image'         => "Use as Coupon image",
      'menu_name'                  => "Coupons",
      'filter_items_list'          => "Filter Coupons list",
      'items_list_navigation'      => "Coupon list navigation",
      'items_list'                 => "Coupons list",
      'item_published'             => "Coupon published.",
      'item_published_privately'   => "Coupon published privately.",
      'item_reverted_to_draft'     => "Coupon reverted to draft.",
      'item_scheduled'             => "Coupon scheduled.",
      'item_updated'               => "Coupon updated."
    ]
  ];
  public $metaboxes = [
    [
      'id'        => "details",
      'context'   => "side",
      'title'     => "Coupon Details",
      'priority'  => "high",
      'fields'    => [
        [
          'id'      => "promo",
          'type'    => "select",
          'options' => [
            'desc'      => "The short name for the promo type in parentheses can be used to limit to a certain type of coupon when using the shortcode to list them on the site.",
            'label'     => "Promo Type",
            'options'   => [
              ''          => "Select a promo type...",
              'discount'  => "Discount (discount)",
              'price'     => "Special Price (price)",
              'bogo'      => "Buy One Get One (bogo)",
              'free'      => "Free / Complimentary (free)"
            ]
          ]
        ],
        [
          'id'      => "value",
          'type'    => "input",
          'options' => [
            'label'     => "Value / Amount",
            'desc'      => "The value of the promotion type, if applicable. Use the phrasing that you'd like to see promoted.  For example, '10% Off' or '\$19.99'",
            'render'    => '<input id="%s" name="%s" value="%s" class="form-control" type="text" placeholder="e.g. $10 off" />',
            'size'      => "small"
          ]
        ],
        [
          'id'      => "expiration",
          'type'    => 'input',
          'options' => [
            'desc'    => "The date this coupon is valid until. (Optional)",
            'label'   => "Expiration Date",
            'render'  => '<input id="%s" name="%s" type="text" placeholder="MM-DD-YY" pattern="/[\d][\d]-[\d][\d]-[\d][\d]/" maxlength="8" value="%s" />'
          ]
        ],
        [
          'id'      => "disclaimer",
          'type'    => "input",
          'options' => [
            'desc'      => "(Optional)",
            'label'     => "Disclaimer",
            'render'    => '<textarea id="%s" name="%s" class="form-control" placeholder="Write a brief disclaimer about the parameters of this offer..." rows="4">%s</textarea>'
          ]
        ]
      ],
      'nonce' => [
        'name'    => "loc_coupon_details",
        'action'  => "save_loc_coupon_details"
      ]
    ]
  ];
  public $useDefaultMeta = false;

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      Coupons can be used to display sales promotions across the site.  The Coupon post uses the <a target="_new" href="/wp-admin/edit-tags.php?taxonomy=category&post_type=coupon">Category taxonomy</a>, which can be used to aggregate similar sales for unique service page and blog purposes.
    </p>
    <p>
      The Coupons posts allow you set details about the type of promotion, the value of the promotion, an expiration date, and a disclaimer that all print to the coupon layout when using the <a href="#shortcode-coupons">Coupons shortcode</a>.  You can use the full Gutenberg editor to build the coupon's content, and you can set a featured image if you want to include a thumbnail that will show in the layout as well.
    </p>
    <div class="alert alert-warning">
      Coupons are not connected to any feed-based data source, but this may be possible in the future.
    </div>
    <a class="btn btn-info btn-sm" href="/wp-admin/edit.php?post_type=coupon">Manage Coupons</a>

    <?php echo ob_get_clean();

  }

}
