<?php

namespace Theme\Parent;

use Theme\Parent\Utility;

class Field {
  
  protected $type;
  protected $id;
  protected $value = "";
  protected $options = [];

  function __construct( $type, $id, $value = "", $options = [] ) {
    
    if( ! ( $id && $type ) || empty( $id ) || empty( $type ) ) {

      error_log( "Missing required $id or $type parameter in " . __NAMESPACE__ . __CLASS__ );
      return false;

    }

    $this->type = $type;
    $this->id = $id;
    $this->value = $value;
    $this->options = $options;

  }

  public function markup() {

    $args = [
      'id'      => $this->id,
      'value'   => $this->value,
      'options' => $this->options
    ];
    Utility::getScopedTemplatePart( "template-parts/field/field", $this->type, $args );

  }
  /**
   * Checkbox Values
   * Resets missing values for saved checkbox fields to `false`
   * so that they are not omitted and fall back to the default
   * value when the markup renders.
   *
   * @param (array) $values: values in the current save process ($_POST)
   * @param (array) $fields: the setup options for the metabox's fields, used to compare the field type
   *
   * @return (array) $values: value array containing omitted unchecked boxes as `false` values.
   */
  public static function checkboxValues( $values, $fields ) {

    if( ! empty( $values ) && is_array( $values ) ) {

      foreach( $fields as $field ) {

        if( "checkbox" === $field['type'] && ! isset( $values[$field['id']] ) )
          $values[$field['id']] = false;

      }
      
    }

    return $values;

  }

}