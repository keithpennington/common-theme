<?php

namespace Theme\Parent\Menu;

use Theme\Parent\{ Menu, Utility };

class Main extends Menu {

  public $slug = "main-nav";
  public $name = "Main Navigation";

  public static function printReference() {

    ob_start() ?>
    <p class="card-text">
      This is the primary menu fixed to the top of the website.  Do not change the menua associated with this location.  Also, be careful about adding new items to this menu.  Newly added menu items will be missing styling notes and may need design updates to sit correctly.
    </p>
    <?php if( isset( self::$menuID ) ) { ?>
    <a class="btn btn-info btn-sm" href="/wp-admin/nav-menus.php?menu=<?php echo self::$menuID ?>">Edit Menu</a>
    <?php } ?>

    <?php echo ob_get_clean();

  }

}
