<?php

namespace Theme\Parent\Menu;

use Theme\Parent\{ Menu, Utility };

class Social extends Menu {

  public $slug = "social";
  public $name = "Social";

  private $_counter = 1;

  protected function _setup() {

    add_action( 'init', [ $this, "setMenuID" ] );
    add_filter( 'wp_nav_menu_args', [ $this, "defaultArgs" ] );

  }

  public function setMenuID() {

    $menus = get_nav_menu_locations();
    if( ! empty( $menus ) && isset( $menus[$this->slug] ) )
      $this->_menuID = $menus[$this->slug];

  }

  public function defaultArgs( $args ) {

    if( ! empty( $args['menu'] ) && $args['menu']->term_id === self::$menuID ) {

      $overrides = [
        'theme_location'  => "social",
        'container'       => false,
        'menu_class'      => "social-items",
        'menu_id'         => "social-nav-{$this->_counter}",
        'echo'            => true,
        'fallback_cb'     => "wp_page_menu",
        'items_wrap'      => '<div id="%1$s" class="%2$s">%3$s</div>',
        'depth'           => 1,
        'walker'          => new \Theme\Parent\Menu\Social\Walker()
      ];
      $args = wp_parse_args( $overrides, $args );
      $this->_counter++;

    }

    return $args;

  }

  public static function printReference() {

    ob_start() ?>

    <p class="card-text">
      Social profile links that can be used throughout the site.
    </p>
    <?php if( isset( self::$menuID ) ) { ?>
    <a class="btn btn-info btn-sm" href="/wp-admin/nav-menus.php?menu=<?php echo self::$menuID ?>">Edit Menu</a>
    <?php } ?>

    <?php echo ob_get_clean();

  }

}

namespace Theme\Parent\Menu\Social;

use Walker_Nav_Menu, Theme\Parent\Utility;

class Walker extends Walker_Nav_Menu {
  /**
   * Valid Item
   * 
   * Used to track whether a nav item meets the spec.
   *
   * @see self::start_el
   */
  private $_valid_item = false;

  /**
   * Strip Default Classes
   *
   * Remove excessive default WP classes that are noisy.
   * Prefer a clean menu markup experience;
   * custom classes added in this Walker.
   *
   * @see self::start_el
   *
   * @param array $classes - Array of classes generated for a WP_Nav_Item
   *
   * @return array $classes
   */
  private function _stripDefaultClasses( $classes ) {

    $classes = array_map( function( $class ) {

      if( preg_match("/(menu-item)/i", $class) )
        return "";

      return $class;

    }, $classes );

    return $classes;

  }

  /**
   * Get Item Attributes
   *
   * Retrieve the anchor tag attributes from the menu item,
   * stringify them for inlining into the tag.
   *
   * @param WP_Post $item - Item object in the Walker
   * @param array $args - Menu arguments used for reference
   *
   * @return string $attributes
   */
  private function _getItemAttributes( $item, $args ): string {

    $atts = [];
    if ( empty( $item->attr_title ) )
      $atts['title'] = ! empty( $item->title ) ? strip_tags( $item->title ) : '';

    $atts['target'] = ! empty( $item->target ) ? $item->target : '';
    $atts['rel']    = ! empty( $item->xfn ) ? $item->xfn : '';
    $atts['href']   = ! empty( $item->url ) ? $item->url : '';
    $atts           = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );
    $attributes     = '';

    foreach ( $atts as $attr => $value ) {

      if ( ! empty( $value ) ) {

        $value      = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
        $attributes .= ' ' . $attr . '="' . $value . '"';

      }

    }

    return $attributes;

  }

  public function start_lvl( &$output, $depth = 0, $args = null ) { }

  public function end_lvl( &$output, $depth = 0, $args = null ) { }

  /**
   * Start Element
   * 
   * Construct nav item html.
   *
   * @see Walker_Nav_Menu::start_el()
   *
   * @param string   $output Used to append additional content (passed by reference).
   * @param WP_Post  $item   Menu item data object.
   * @param int      $depth  Depth of menu item. Used for padding.
   * @param stdClass $args   An object of wp_nav_menu() arguments.
   * @param int      $id     Current item ID.
   *
   * @link https://developer.wordpress.org/reference/classes/walker_nav_menu/
   */
  public function start_el( &$output, $item, $depth = 0, $args = null, $id = 0 ) {

    if( preg_match("/^https?[:\/]{0,3}([\w.]+)\.[\w]{2,}/im", $item->url, $matches ) ) {

      $defaultClasses     = [ "external", "social-item" ];
      $addedClasses       = [];
      $iconClasses        = [];
      $item->classes      = array_filter( $this->_stripDefaultClasses( $item->classes ) );

      if( ! empty( $item->classes ) ) {

        foreach( $item->classes as $class ) {

          if( preg_match( Utility::$faExp, $class ) )
            $iconClasses[] = $class;
          else
            $addedClasses[] = $class;

        }

      } else {

        $match = array_pop($matches);

        $iconClasses = [
          "fab",
          "fa-lg",
          "fa-fw",
          "fa-{$match}"
        ];

      }

      $itemClasses    = array_merge( $defaultClasses, $addedClasses );
      $itemClasses    = implode( " ", apply_filters( 'nav_menu_css_class', $itemClasses, $item, $args, $depth ) );
      $itemAttributes = $this->_getItemAttributes( $item, $args );

      $this->_valid_item  = true;
      $item->title        = sprintf( '<i class="%s" aria-hidden="true"><span class="screen-reader-text">%s</span></i>', implode( " ", $iconClasses ), $item->title );
      $output            .= sprintf( '<a class="%s" href="%s" %s>%s', $itemClasses, $item->url, $itemAttributes, $item->title );

    } else {

      $this->_valid_item = false;

    }

  }
  /**
   * Ends the element output, if needed.
   *
   * @see Walker_Nav_Menu::end_el()
   *
   * @param string   $output Used to append additional content (passed by reference).
   * @param WP_Post  $item   Page data object. Not used.
   * @param int      $depth  Depth of page. Not Used.
   * @param stdClass $args   An object of wp_nav_menu() arguments.
   *
   * @link https://developer.wordpress.org/reference/classes/walker_nav_menu/
   */
  public function end_el( &$output, $item, $depth = 0, $args = null ) {

    if( $this->_valid_item )
      $output .= "</a>\n";

  }

}
