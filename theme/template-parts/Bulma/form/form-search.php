<?php
/**
 * Template for Search Widget
 * 
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

ob_start() ?>

<form role="search" aria-label="Search Site" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ) ?>">
  <div class="field has-addons">
    <div class="control input-wrapper">
      <label>
        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
        <input type="search" class="input search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" />
      </label>
    </div>
    <div class="control submit-wrapper">
      <input type="submit" class="button is-info search-submit" value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
    </div>
  </div>
</form>

<?php echo ob_get_clean();
