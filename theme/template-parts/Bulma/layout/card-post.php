<?php
/**
 * Card for a blog excerpt and author
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args) )
  exit;

use Theme\Parent\Utility;

$author = get_the_author();
$email  = get_the_author_meta( "email" );
$avatar = get_avatar( $email, 96, "", $author, [ 'scheme' => "https", 'class' => "is-rounded" ] );
$date   = get_the_date();
$image  = get_the_post_thumbnail_url( get_the_ID(), "tablet" );

ob_start() ?>

<article class="card post-card">
  <?php if( $image ) { ?>
  <header class="card-image">
    <a href="<?php the_permalink() ?>">
      <figure class="image is-16by9 allow-overflow">
        <img class="landscape-oriented" src="<?php echo $image ?>" alt="<?php echo Utility::getFeaturedImgAltText() ?>">
      </figure>
    </a>
  </header>
  <?php } ?>
  <section class="card-content">
    <figure class="author-avatar image is-64x64">
      <?php echo $avatar ?>
    </figure>
    <div class="card-title">
      <a href="<?php the_permalink() ?>">
        <h3 class="title is-5"><?php the_title() ?></h3>
      </a>
    </div>
    <main class="content">
      <?php the_excerpt() ?>
      <p class="read-more">
        <a href="<?php the_permalink() ?>">READ MORE &raquo;</a>
      </p>
    </main>
  </section>
  <footer class="card-footer">
    <span class="post-author"><?php echo $author ?></span> 
    &middot; 
    <time datetime="<?php echo $date ?>"><?php echo $date ?></time>
  </footer>
</article>

<?php echo ob_get_clean();
