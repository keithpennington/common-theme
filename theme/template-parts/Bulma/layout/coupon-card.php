<?php
/**
 * Card layout for coupons
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

use Theme\Parent\Utility;

$details    = Utility::getPostMetaValue( "details" );
$disclaimer = isset( $details['disclaimer'] ) && ! empty( $details['disclaimer'] ) ? $details['disclaimer'] : false;
$expiration = isset( $details['expiration'] ) && ! empty( $details['expiration'] ) ? $details['expiration'] : false;
$hasFooter  = $disclaimer || $expiration;

ob_start() ?>

<div class="card coupon mx-1">
  <header class="card-header is-flex-wrap-wrap py-3 px-5">
    <h4 class="card-title title is-4">
      <?php the_title() ?>
    </h4>
    <?php if( isset( $details['value'] ) && ! empty( $details['value'] ) ) { ?>
    <span class="coupon-promo">
      <?php echo $details['value'] ?>
    </span>
    <?php } ?>
  </header>
  <?php if( has_post_thumbnail() ) { ?>
  <div class="card-image">
    <figure class="image is-16by9">
      <img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), "large-mobile" ) ?>" alt="<?php echo get_post_meta( get_the_ID(), "_wp_attachment_image_alt", true ) ?>">
    </figure>
  </div>
  <?php } ?>
  <section class="card-content">
    <?php the_content() ?>
  </section>
  <?php if( $hasFooter ) { ?>
  <footer class="card-footer level is-flex-wrap-wrap py-3 px-5">
    <?php

      if( $disclaimer )
        echo sprintf( "\t\t<div class='level-left disclaimer'>%s</div>\n", $disclaimer );
      if( $expiration )
        echo sprintf( "\t\t<div class='level-right expiration'>%s %s</div>\n", apply_filters( 'coupon_expiry_label', "Expires:" ), $expiration );

    ?>
  </footer>
  <?php } ?>
</div>

<?php echo ob_get_clean();
