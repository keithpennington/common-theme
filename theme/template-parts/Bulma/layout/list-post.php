<?php
/**
 * Media object for a blog excerpt
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args) )
  exit;

$dateFormat = get_option( 'date_format', "M j, Y" );
$timeFormat = get_option( 'time_format', "h:i A" );
$datetime   = get_the_date( "l m-d-Y " . $timeFormat, $args['id'] );
$date       = get_the_date( $dateFormat, $args['id'] );

$postClasses = [
  "media"
];

?>

<article class="<?php echo implode( " ", apply_filters( 'list_post_classes', $postClasses ) ) ?>">
  <figure class="media-left">
    <?php if( has_post_thumbnail( $args['id'] ) ) { ?>
    <p class="image is-64x64">
      <a href="<?php the_permalink() ?>">
        <?php echo get_the_post_thumbnail( $args['id'], [ 64,64 ] ) ?>
      </a>
    </p>
    <?php } ?>
  </figure>
  <div class="media-content">
    <div class="content">
      <a href="<?php the_permalink() ?>">
        <h4 class="title is-4">
          <?php

          the_title();
          echo sprintf(
            '<small class="%s" title="%s">%s%s</small>',
            "ml-2 is-size-6 is-bold has-text-weight-normal",
            $datetime,
            '<span class="screen-reader-text">' . __("Published on") . ' </span> ',
            $date
          );
          
          ?>
        </h4>
      </a>
      <?php the_excerpt() ?>
    </div>
    <nav class="level is-mobile">
      <div class="level-left">
        <a class="ml-1 level-item read-more" href="<?php the_permalink() ?>">
          Read more
          <span class="screen-reader-text"><?php echo __("about", "parent.theme") . " " . get_the_title() ?></span>
        </a>
      </div>
    </nav>
  </div>
  <div class="media-right">
  </div>
</article>
