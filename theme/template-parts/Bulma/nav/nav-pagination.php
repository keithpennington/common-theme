<?php
/**
 * Pagination
 * 
 * @param $args['classes'] string Additional classes for this pagination nav.
 * @param $args['items'] WP_items result of items matching the 'type' passed to the shortcode.
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

global $wp_rewrite, $wp_query;

$classes      = isset( $args['classes'] ) ? $args['classes'] : [];
$items        = isset( $args['items'] ) ? $args['items'] : $wp_query;

$pagenum_link = html_entity_decode( get_pagenum_link() );
$url_parts    = explode( '?', $pagenum_link );
$total        = isset( $items->max_num_pages ) ? $items->max_num_pages : 1;

$current      = 0 !== get_query_var( 'paged' ) ? intval( get_query_var('paged') ) : 1;
$pagenum_link = trailingslashit( $url_parts[0] ) . '%_%';

$format       = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
$format      .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';
$base         = $pagenum_link;

$navItems = [];

// Previous Button
$disabled = "";

if( $current && 1 < $current ) {

  $link     = str_replace( '%_%', 2 == $current ? '' : $format, $base );
  $link     = str_replace( '%#%', $current - 1, $link );

} else {

  $link           = "#";
  $disabled       = 'tabindex="-1" aria-disable="true" disabled';

}

$navItems[] = sprintf(
  '<a class="pagination-previous" href="%s" %s>Prev</a>',
  $link,
  $disabled
);

// Next Button
$disabled     = "";

if( $current && $current < $total ) {

  $link   = str_replace( '%_%', $format, $base );
  $link   = str_replace( '%#%', $current + 1, $link );

} else {

  $link       = "#";
  $disabled     = 'tabindex="-1" aria-disable="true" disabled';

}

$navItems[] = sprintf(
  '<a class="pagination-next" href="%s" %s>Next</a>',
  $link,
  $disabled
);

$navItems[] = '<ul class="pagination-list">';

// Index buttons
for( $n = 1; $n <= $total; $n++ ) {

  $pageClasses  = [ "pagination-link" ];
  $link         = str_replace( '%_%', 1 == $n ? '' : $format, $base );
  $link         = str_replace( '%#%', $n, $link );
  $ariaLabel    = "Go to page {$n}";
  $ariaCurrent  = "";
  $label        = number_format_i18n( $n );

  if( $n == $current ) {

    $pageClasses[]  = "is-current";
    $aria           = "Page {$n}";
    $label         .= ' <span class="sr-only">(current)</span>';
  
  }

  $navItems[] = sprintf(
    "<li>%s</li>",
    sprintf(
      '<a class="%s" href="%s" aria-label="%s" %s>%s</a>',
      implode( " ", $pageClasses ),
      $link,
      $ariaLabel,
      $ariaCurrent,
      $label
    )
  );

}

$navItems[] = "</ul>";

$output = sprintf(
  '<nav class="pagination %s" role="navigation" aria-label="pagination">%s</nav>',
  implode( " ", apply_filters( 'pagination_classes', $classes, $items ) ),
  implode( "\n", $navItems )
);

echo apply_filters( "html_pagination", $output, $navItems );
