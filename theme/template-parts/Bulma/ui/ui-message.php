<?php

use Theme\Parent\Utility;

if( ! isset( $args ) )
  exit;

$dismissable = "disallowed" !== $args['dismiss_behavior'];

$classes = [
  "message",
  $args['style'],
  $args['size']
];

if( $dismissable )
  $classes[] = "dismissable";

ob_start() ?>

<article
  id="message-<?php echo get_the_ID() ?>"
  class="<?php echo implode( " ", apply_filters( 'message_classes', $classes, $args ) ) ?>"
  data-remember="<?php echo "remember" === $args['dismiss_behavior'] ? "true" : "false" ?>"
>
  <div class="message-header">
    <p><?php the_title() ?></p>
    <?php if( $dismissable ) { ?>
    <button class="delete <?php echo $args['size'] ?>" aria-label="delete"></button>
    <?php } ?>
  </div>
  <div class="message-body">
    <?php the_content() ?>
  </div>
</article>

<?php echo ob_get_clean();
