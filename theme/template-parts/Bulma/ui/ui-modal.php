    <div id="basic-modal" class="modal">
      <div class="modal-background"></div>
      <div class="modal-content"></div>
      <button class="modal-close is-large" aria-label="close"></button>
    </div>

    <div id="image-modal" class="modal">
      <div class="modal-background"></div>
      <div class="modal-content">
        <div class="image"></div>
      </div>
      <button class="modal-close is-large" aria-label="close"></button>
    </div>

    <div id="card-modal" class="modal">
      <div class="modal-background"></div>
      <div class="modal-card">
        <header class="modal-card-head">
          <p class="modal-card-title"></p>
          <button class="delete" aria-label="close"></button>
        </header>
        <section class="modal-card-body"></section>
        <footer class="modal-card-foot">
          <button class="button">Close</button>
        </footer>
      </div>
    </div>