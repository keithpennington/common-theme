<?php
/**
 * Full-height hero that will contain entire page's content.
 * 
 * @param $args['classes'] array Additional classes to add to the <section> container.
 * @param $args['image'] bool Whether or not to show the featured image on this post. Default is has_post_thumbnail().
 * @param $args['title'] string The title that should be printed to the page. Default is get_the_title().
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

use Theme\Parent\Utility;

$defaults = [
  'classes' => [],
  'image'   => has_post_thumbnail(),
  'title'   => get_the_title()
];
$hero             = wp_parse_args( $args, apply_filters( 'hero_default_args', $defaults ) );
$standardClasses  = [ "hero", "is-fullheight", "is-bold", \Theme\Parent\Framework\Bulma::getDefaultStyle( "hero-full" ) ];
$classes          = array_merge( apply_filters( 'hero_standard_classes', $standardClasses ), $hero['classes'] );
$title            = [];

if( false === $hero['image'] ) {

  $classes[] = "no-image";

} else {

  $classes[]  = "has-image";
  $style      =  Utility::getHeaderBackgroundStyle( get_the_ID(), $classes );

}

ob_start();

if( isset( $style ) )
  echo $style;

?>

<section class="<?php echo implode( " ", $classes ) ?>">
  <?php do_action( 'hero_inside_header' ) ?>
  <div class="hero-body">
    <div class="container content">
      <?php the_content() ?>
    </div>
  </div>
</section>

<?php echo ob_get_clean();
