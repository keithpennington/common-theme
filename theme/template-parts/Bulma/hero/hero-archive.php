<?php
/**
 * Hero that received a title based on the queried object.
 * 
 * @param $args['classes'] array Additional classes to add to the <section> container.
 * @param $args['image'] bool Whether or not to show the featured image on this post. Default is has_post_thumbnail().
 * @param $args['title'] string The title that should be printed to the page. Default is get_the_title().
 * @param $args['subtitle'] string The subtitle that should be printed to the page. Default is blank.
 * @param $args['description'] string The description provided to the relative taxonomy.
 * @param $args['hero_slider'] string The slug/name of an active slider to use.
 *
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

// use \Theme\Parent\Utility;

$postsPageID  = get_option( 'page_for_posts' );
$requiredClasses      = [
  "entry-header",
  "hero",
  "is-medium"
];
$requiredClasses[]    = \Theme\Parent\Framework\Bulma::getDefaultStyle( "hero" );
$args['classes']      = isset( $args['classes'] ) ? $args['classes'] : [];
$args['title']        = isset( $args['title'] ) ? $args['title'] : get_the_title( $postsPageID );
$titles               = [];
$titles[]             = sprintf( '<h1 class="%s">%s</h1>', apply_filters( 'hero_title_class', "is-1 entry-title title m-0"), $args['title'] );

if( isset( $args['subtitle'] ) )
  $titles[] = sprintf( '<h2 class="is-4 is-bold entry-subtitle subtitle my-3">%s</h2>', $args['subtitle'] );

$classes  = array_merge( $requiredClasses, $args['classes'] );

ob_start() ?>

  <div class="hero-body">
    <div class="container">
        <?php echo implode( "\n", $titles ) ?>
      <?php

        if( isset( $args['subtitle'] ) && ! empty( $args['subtitle'] ) )
          echo sprintf('<h2 class="subtitle is-2">%s</h2>', $args['subtitle'] );

        if( isset( $args['description'] ) && ! empty( $args['description'] ) )
          echo sprintf( '<p class="description is-small">%s</p>', $args['description'] );
      ?>
    </div>
  </div>

<?php $defaultBody = ob_get_clean();

ob_start();

?>

<header id="main-hero" class="<?php echo implode( " ", apply_filters( 'hero_classes', $classes ) ) ?>">
 <?php

  do_action( 'hero_inside_header' );

  if( isset( $args['hero_slider'] ) ) { ?>
    
    <div class="hero-body p-0">
      <?php echo do_shortcode( "[slider name='{$args['hero_slider']}']{$defaultBody}[/slider]" ) ?>
    </div>

  <?php } else {

    echo $defaultBody;

  } ?>
  </div>
</header>

<?php echo ob_get_clean();
