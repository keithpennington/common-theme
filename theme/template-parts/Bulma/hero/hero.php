<?php
/**
 * Header to display full-width featured image
 * 
 * @param $args['after'] string text/HTML to render inside the h1 tag, after the title.
 * @param $args['before'] string text/HTML to render inside the h1 tag, before the title.
 * @param $args['classes'] array Additional classes to add to the <header> container.
 * @param $args['image'] bool Whether or not to show the featured image on this post. Default is has_post_thumbnail().
 * @param $args['title'] string The title that should be printed to the page. Default is get_the_title().
 * @param $args['hero_slider'] string The slug/name of an active slider to use.
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

use Theme\Parent\Utility;

$defaults           = [
  'after'     => "",
  'before'    => "",
  'classes'   => [],
  'image'     => apply_filters( 'use_hero_image', has_post_thumbnail(), $args ),
  'title'     => get_the_title(),
  'subtitle'  => ""
];
$standardClasses    = [
  "entry-header",
  "hero",
  "is-medium"
];
$hero               = wp_parse_args( $args, apply_filters( 'hero_default_args', $defaults ) );
$heroSettings       = get_post_meta( get_the_ID(), '_hero_settings', true );
$heroStyle          = ! isset( $heroSettings['hero_style'] ) || empty( $heroSettings['hero_style'] ) ? \Theme\Parent\Framework\Bulma::getDefaultStyle( "hero" ) : $heroSettings['hero_style'];
$standardClasses[]  = $heroStyle;
$classes            = array_merge( $standardClasses, $hero['classes'] );
$classes[]          = false === $hero['image'] ? "no-image" : "has-image";
$classes            = apply_filters( 'hero_classes', $classes );
$title              = [];

foreach( [ "before", "title", "after" ] as $placement ) {

  if( ! empty( $hero[$placement] ) )
    $title[] = $hero[$placement];

}

ob_start() ?>

  <div class="hero-body">
    <div class="container">
      <h1 class="<?php echo apply_filters( 'hero_title_class', "entry-title title is-1" ) ?>">
        <?php echo implode( "\n", $title ) ?>
      </h1>
      <?php
        if( ! empty( $hero['subtitle'] ) )
          echo sprintf('<h2 class="subtitle is-4">%s</h2>', $hero['subtitle'] );
      ?>
    </div>
  </div>

<?php $defaultBody = ob_get_clean();

ob_start();

if( false !== $hero['image'] )
  echo Utility::getHeroBgImageStyles( get_the_ID(), $classes );

?>

<header id="main-hero" class="<?php echo implode( " ", $classes ) ?>">
  <?php

  do_action( 'hero_inside_header' );

  if( isset( $args['hero_slider'] ) ) { ?>
    
    <div class="hero-body p-0">
      <?php echo do_shortcode( "[slider name='{$args['hero_slider']}']{$defaultBody}[/slider]" ) ?>
    </div>

  <?php } else {

    echo $defaultBody;

  } ?>
</header>

<?php echo ob_get_clean();
