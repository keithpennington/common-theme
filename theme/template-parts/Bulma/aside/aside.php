<?php
/**
 * Template for single sidebar
 * 
 * @param $args['sidebar'] string The name of the active sidebar that should be used as the content.
 * @param $args['classes'] array Additional classes to add to the <aside> container.
 * 
 * @package Lights On Creative
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

$args['sidebar'] = isset( $args['sidebar'] ) && ! empty( $args['sidebar'] ) ? $args['sidebar'] : false;
$args['classes'] = isset( $args['classes'] ) ? $args['classes'] : [];

if( is_active_sidebar( $args['sidebar'] ) ) {

  $defaultClasses  = [ "sidebar", "content", "sidebar-{$args['sidebar']}" ];

  $classes  = array_merge( $defaultClasses, $args['classes'] );
  $hookArgs = [
    'sidebar'   => $args['sidebar'],
    'framework' => "bulma"
  ];

  ob_start() ?>

  <aside id="main-sidebar" class="<?php echo implode( " ", apply_filters( 'sidebar_class', $classes, $hookArgs ) ) ?>">
    <?php do_action( 'html_before_sidebar', $hookArgs ) ?>
    <section class="section px-1">
      <?php dynamic_sidebar( $args['sidebar'] ) ?>
    </section>
    <?php do_action( 'html_after_sidebar', $hookArgs ) ?>
  </aside>

  <?php echo ob_get_clean();

}
