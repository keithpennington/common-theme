<?php
/**
 * Panel formatting for the Linktree shortcode
 * 
 * @param $args['classes'] array Additional classes to add to the <article> container.
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

if( ! isset( $args ) )
  exit;

?>
<article class="panel <?php echo $args['classes'] ?>">
  <h3 class="panel-heading">
    Primary
  </h3>
  <a class="panel-block is-active">
    <span class="panel-icon">
      <i class="fas fa-book" aria-hidden="true"></i>
    </span>
    bulma
  </a>
  <a class="panel-block">
    <span class="panel-icon">
      <i class="fas fa-book" aria-hidden="true"></i>
    </span>
    marksheet
  </a>
  <a class="panel-block">
    <span class="panel-icon">
      <i class="fas fa-book" aria-hidden="true"></i>
    </span>
    minireset.css
  </a>
  <a class="panel-block">
    <span class="panel-icon">
      <i class="fas fa-book" aria-hidden="true"></i>
    </span>
    jgthms.github.io
  </a>
</article>
