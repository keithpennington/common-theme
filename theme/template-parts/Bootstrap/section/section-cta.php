<?php
/**
 * The template for displaying CTA sections on Offerings
 *
 * Template: CTA Section
 * $tag (string) The HTML tag to use in rendering the section. Default "div".
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

global $post;

$tag = isset( $args['tag'] ) && ! empty( $args['tag'] ) ? $args['tag'] : "div";
$id = isset( $args['id'] ) && ! empty( $args['id'] ) ? $args['id'] : "";
$container = isset( $args['container'] ) && ! empty( $args['container'] ) ? $args['container'] : "container-full";
$classes = isset( $args['classes'] ) && ! empty( $args['classes'] ) ? implode( " ", $args['classes'] ) : implode( " ", [ "offering-ctas", $post->post_type ] );
$cta_block = get_post_meta( $post->ID, 'cta', true );

echo sprintf( '<%s id="%s" class="%s">', $tag, $id, $classes );
  echo sprintf( '<div class="%s">', $container );
    echo $cta_block;
  echo sprintf( "</div>" );
echo sprintf( '</%s>', $tag );
