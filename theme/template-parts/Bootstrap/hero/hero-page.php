<?php
/**
 * Header to display full-width featured image
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility as Utility;

$class    = "entry-header";
$icon     = isset( $args['icon'] ) && ! empty( $args['icon'] ) ? $args['icon'] : false;
$image    = isset( $args['image'] ) && ! empty( $args['image'] ) ? $args['image'] : ! empty( get_the_post_thumbnail( get_the_ID() ) ) ? true : false;
$title    = [ get_the_title() ];
$classes  = [ $class ];
$contents   = [];

if( false === $image )
  $classes[] = "no-image";
else
  $style =  Utility::getHeaderBackgroundStyle( get_the_ID(), $class );

if( false !== $icon )
  $title[] = sprintf( '<i class="fa fa-fw %s"></i>', $icon );

$contents[] = sprintf(
  '<h1 class="entry-title">%s</h1>',
  implode( "\n", $title )
);

echo $style;

echo sprintf(
  '<header class="%s">%s</header>',
  implode( " ", $classes ),
  implode( "\n", $contents )
);
