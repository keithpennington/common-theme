<?php

use Theme\Parent\{ Field, Utility };

if( ! isset( $args ) )
  exit;

$nonce = isset( $args['nonce'] ) ? $args['nonce'] : false;

if( false === $nonce )
  exit;

$values = get_term_meta( $args['term']->term_id, "_" . $args['id'], true );

ob_start(); ?>

<div class="card term-metabox" id="<?php echo $args['id'] ?>" style="padding:0">
  <h6 class="card-header">
    <?php echo $args['title'] ?>
  </h6>
  <div class="card-body">
    <?php

    if( is_array( $args['fields'] ) && ! empty( $args['fields'] ) ) {

      wp_nonce_field( $args['nonce']['action'], $args['nonce']['name'] );

      foreach( $args['fields'] as $field ) {

        if( ! isset( $field['default'] ) )
          $field['default'] = "";

        $value        = isset( $values[$field['id']] ) ? $values[$field['id']] : $field['default'];
        $id           = sprintf( "%s[%s]", $args['id'], $field['id'] );
        $renderField  = new Field( $field['type'], $id, $value, $field['options'] );

        if( $renderField )
          $renderField->markup();

      }

    }

    ?>
  </div>
</div>

<?php echo ob_get_clean();
