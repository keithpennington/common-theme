<?php
/**
 * @param string $args[id]
 * @param string $args[value]
 * @param  array $args[options]
 * $options = [
 *   'class',
 *   'label',
 *   'desc'
 * ];
 *
 * @see parent.theme/classes/Field.php
 * @link https://getbootstrap.com/docs/4.2/components/forms/#overview
 */

if( ! isset( $args['id'] ) )
  exit;

use Theme\Parent\Utility;

$options  = isset( $args['options'] ) ? $args['options'] : [];
$label    = isset( $options['label'] ) ? $options['label'] : "";
$desc     = isset( $options['desc'] ) ? sprintf( '<p class="description">%s</p>', $options['desc'] ) : "";
$checked  = isset( $args['value'] ) && in_array( $args['value'], Utility::trueTerms() ) ? "checked " : "";
$classes  = [
  "form-group",
  "form-check",
  "custom-field-wrapper"
];

if( isset( $options['class'] ) )
  $classes = array_unique( array_merge( $classes, explode( " ", $options['class'] ) ) );

echo sprintf( '<div id="field-%s-wrapper" class="%s">', $args['id'], implode( " ", $classes ) );

echo sprintf(
  '<input type="checkbox" class="form-check-input" id="%s" name="%s" %s/>',
  $args['id'],
  $args['id'],
  $checked,
);

if( ! empty( $label ) ) {

  echo sprintf(
    '<label class="form-check-label" for="%s">%s</label>%s',
    $args['id'],
    $label,
    $desc
  );

} else
  echo $desc;


echo "</div>";
