<?php
/*
 * @param string $args[id]
 * @param string $args[value]
 * @param  array $args[options]
 * $options = [
 *   'class',
 *   'desc',
 *   'options' => [ 'value' => 'label' ],
 *   'options_cb',
 *   'size'
 * ];
 *
 */

if( ! isset( $args['id'] ) )
  exit;

$options      = isset( $args['options'] ) ? $args['options'] : [];
$describedby  = isset( $options['desc'] ) ? sprintf( 'aria-describedby="%s-description"', $args['id'] ) : '';
$desc         = isset( $options['desc'] ) ? "<p class='description'>{$options['desc']}</p>" : "";
$size         = isset( $options['size'] ) ? $options['size'] : "regular";
$class        = isset( $options['class'] ) ? $options['class'] : "";

if( isset( $options['options'] ) )
  $selectOptions = $options['options'];
elseif( isset( $options['options_cb'] ) ) {

  if( is_array( $options['options_cb'] ) ) {

    if( is_object( $options['options_cb'][0] ) ) {

      $selectOptions = call_user_func( $options['options_cb'] );

    } elseif( isset( $options['options_cb'][0] ) && isset( $options['options_cb'][1] ) && method_exists( $options['options_cb'][0], $options['options_cb'][1] ) ) {

      $obj = $options['options_cb'][0]::instance();
      $selectOptions = call_user_func( [ $obj, $options['options_cb'][1] ], get_the_ID() );

    }

  } elseif( is_string( $options['options_cb'] ) && is_callable( $options['options_cb'] ) ) {

    $selectOptions = $options['options_cb']();

  }

}

$output  = "<div class='form-group custom-field-wrapper {$class}'>";

if( isset($options['label'] ) )
  $output .= sprintf( '<label for="%s">%s</label>', $args['id'], $options['label'] );

$output .= sprintf(
  '<select class="custom-select %s-dropdown" id="%s" name="%s" %s>',
  $size,
  $args['id'],
  $args['id'],
  $describedby
);

if( isset( $selectOptions ) ) {

  foreach( $selectOptions as $value => $label ) {

    $selected  = $value == $args['value'] ? "selected" : "";
    $output   .= sprintf(
      '<option value="%s" %s>%s</option>',
      $value,
      $selected,
      $label
    );

  }

}

$output .= "</select>";
$output .= $desc;
$output .= "</div>";

echo $output;
