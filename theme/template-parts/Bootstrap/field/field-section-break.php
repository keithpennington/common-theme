<?php
/*
 * @param string $args[id]
 * @param  array $args[options]
 * $options = [
 *   'desc',
 *   'class',
 *   'title',
 *   'html'
 * ];
 */

if( ! isset( $args['id'] ) )
  exit;

$options  = isset( $args['options'] ) ? $args['options'] : [];
$class    = isset( $options['class'] ) ? $options['class'] : "";
$title    = isset( $options['title'] ) ? $options['title'] : "";
$desc     = isset( $options['desc'] ) ? $options['desc'] : "";
$html     = isset( $options['html'] ) ? $options['html'] : "";

echo sprintf( '<h5 id="%s" class="%s">%s</h5>', $args['id'], $options['class'], $options['title'] );

if( ! empty( $desc ) )
  echo sprintf( '<p class="description">%s</p>', $desc );

if( ! empty( $html ) )
  echo $html;
