<?php
/*
 * @param string $args[id]
 * @param string $args[value]
 * @param  array $args[options]
 * $options = [
 *   'class',
 *   'desc',
 *   'pattern',
 *   'placeholder',
 *   'size',
 *   'title'
 * ];
 *
 */

if( ! isset( $args['id'] ) )
  exit;

use Theme\Parent\Utility;

$htmlAtts = [
  "class",
  "pattern",
  "placeholder",
  "title"
];
$options          = isset( $args['options'] ) ? $args['options'] : [];
$options['size']  = isset( $options['size'] ) ? $options['size'] : "regular";
$classes          = isset( $options['class'] ) ? explode( " ", $options['class'] ) : [];
$classes          = array_merge( $classes, [ "form-control", $options['size'] . "-text" ] );
$options['class'] = implode( " ", array_unique( $classes ) );

$additionalAtts = Utility::stringifyHtmlAttributes( $options, $htmlAtts );
$describedby = isset( $options['desc'] )  ? 'aria-describedby="' . $args['id'] . '-description"' : '';

echo sprintf(
  '<input type="text" name="%s" %s %s value="%s"/>',
  $args['id'],
  $additionalAtts,
  $describedby,
  $args['value'],
);

if( isset( $options['desc'] ) )
  echo sprintf( '<p id="%s-description" class="description">%s</p>', $args['id'], $options['desc'] );
