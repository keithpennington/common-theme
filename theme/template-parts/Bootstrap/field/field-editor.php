<?php
/*
 * @param string $args[id]
 * @param string $args[value]
 * @param  array $args[options]
 * $options = [
 *   'container',
 *   'label',
 *   'class'
 * ];
 *
 */

if( ! isset( $args['id'] ) )
  exit;

$options = isset( $args['options'] ) ? $args['options'] : [];

if( ! isset( $options['container'] ) || empty( $options['container'] ) )
  $options['container'] = "div";

if( ! isset( $options['class'] ) )
  $options['class'] = "";

echo sprintf( '<%s id="field-%s-wrapper" class="custom-field-wrapper">', $options['container'], $args['id'] );

if( isset( $options['label'] ) )
  echo sprintf( '<label for="%s_ifr">%s</label>', $args['id'], $options['label'] );

wp_editor(
  $value,
  $id,
  $options
);

if ( isset( $options['desc'] ) )
  echo sprintf( '<p class="description">%s</p>', $options['desc'] ); 

echo sprintf("</%s>", $options['container']);
