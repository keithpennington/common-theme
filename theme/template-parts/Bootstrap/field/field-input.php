<?php
/*
 * @param string $args[id]
 * @param string $args[value]
 * @param  array $args[options]
 * $options = [
 *   'class',
 *   'container',
 *   'desc',
 *   'label',
 *   'render'
 * ];
 *
 */

if( ! isset($args['id'] ) )
  exit;

$options      = isset( $args['options'] ) ? $args['options'] : [];
$defaultClass = [ "custom-field-wrapper" ];
$classes      = isset( $options['class'] ) ? array_merge( $defaultClass, explode( " ", $options['class'] ) ) : $defaultClass;

if( ! isset( $options['container'] ) )
  $options['container'] = "div";

if( isset( $options['container'] ) )
  echo sprintf( '<%s id="field-%s-wrapper" class="%s">', $options['container'], $args['id'], implode( " ", $classes ) );

if( isset( $options['label'] ) )
  echo sprintf( '<label for="%s">%s</label>', $args['id'], $options['label'] );

echo sprintf( $options['render'], $args['id'], $args['id'], $args['value'] );

if( isset( $options['desc'] ) )
  echo sprintf( '<p class="description">%s</p>', $options['desc'] );

if( isset( $options['container'] ) )
  echo sprintf( '</%s>', $options['container'] );
