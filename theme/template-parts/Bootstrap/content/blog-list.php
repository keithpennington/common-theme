<?php
/**
 * @package WordPress
 * @subpackage siteTheme
 */

$posts = isset( $args['posts'] ) && ! empty( $args['posts'] ) ? $args['posts'] : false;

if( false !== $posts ): ?>

  <ul class="list-unstyled">
    <?php while( $posts->have_posts() ):

      $posts->the_post(); ?>
      <li class="media-item card soft-drop hover-increase-drop hover-lift">
        <div class="media">
          <?php echo get_the_post_thumbnail( get_the_ID(), [64,64], ['class' => "mr-3 align-self-top"] ); ?>
          <div class="media-body">
            <h4 class="mt-0 mb-2">
              <a href="<?php the_permalink() ?>">
                <?php the_title() ?>
              </a>
              <small><?php echo get_the_date( 'M n, Y', get_the_ID() ) ?></small>
            </h4>
            <?php the_excerpt() ?><p><a href="<?php the_permalink() ?>">[ read more ]</a></p>
          </div>
        </div>
      </li>
    <?php endwhile; ?>
  </ul>

<?php endif ?>
