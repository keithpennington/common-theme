<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

use Theme\Parent\Utility;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container-full">
		<div class="row no-gutters">
			<div class="col">

				<?php Utility::getScopedTemplatePart(
					"template-parts/header/header",
					"page"
				); ?>

				<div class="entry-content">
					<?php the_content(); ?>
				</div>
				
			</div>
		</div>
	</div>
</article>
