<?php
/**
 * @package WordPress
 * @subpackage LOC Parent Theme
 */

$sidebar = isset( $args['sidebar'] ) && ! empty( $args['sidebar'] ) ? $args['sidebar'] : false;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="container-full">
		<div class="row no-gutters">
			<div class="col">

				<header class="entry-header">
					<?php echo get_the_post_thumbnail( get_the_ID(), 'full', ['height' => '', 'width' => ''] ); ?>
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header>

				<div class="entry-content has-sidebar">

					<?php if( false !== $sidebar ): ?>
						<aside id="sidebar-<?php echo $sidebar ?>" class="page-sidebar">
							<?php dynamic_sidebar( $sidebar ) ?>
						</aside>
					<?php endif; ?>

					<?php the_content(); ?>
					
				</div>
				
			</div>
		</div>
	</div>
</article>
