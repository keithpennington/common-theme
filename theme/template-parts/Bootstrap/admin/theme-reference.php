<?php

if( ! isset( $args['id'] ) )
  exit;

use Theme\Parent\Utility;

$page_title     = isset( $args['page_title'] ) && ! empty( $args['page_title'] ) ? $args['page_title'] : "";
$referenceItems = apply_filters( 'theme_reference_items', [ ] );
$dropdowns      = [
  'shortcodes'  => "Shortcodes",
  'postTypes'   => "Post Types",
  'menus'       => "Menus"
];

?>

<div class="container mt-3 pr-md-5" id="theme-reference">
  <div class="row">
    <div class="col">
      <h1 class="display-6">Theme Reference</h1>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <nav class="navbar navbar-expand-lg navbar-light rounded" style="background:#F4C206">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#guide-nav" aria-controls="guide-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div id="guide-nav" class="collapse navbar-collapse">
          <ul class="navbar-nav mr-auto">
            <?php

            foreach( $dropdowns as $slug => $title ) {

              if( isset( $referenceItems[$slug] ) && ! empty( $referenceItems[$slug] ) ) { ?>

            <li class="m-0 nav-item dropdown active">
              <a class="nav-link dropdown-toggle" href="#" id="<?php echo $slug . "Dropdown" ?>" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $title ?></a>
              <div class="dropdown-menu" aria-labelledby="<?php echo $slug . "Dropdown" ?>">
                <?php foreach( $referenceItems[$slug] as $item ) {

                  echo sprintf(
                    '<a class="dropdown-item" href="#shortcode-%s">%s</a>',
                    $item['slug'],
                    $item['name']
                  );

                } ?>
              </div>
            </li>
              <?php }

            }

            ?>
            <li class="m-0 nav-item active">
              <a class="nav-link" href="#sidebars">Sidebars</a>
            </li>
            <li class="m-0 nav-item active">
              <a class="nav-link" href="#media">Media</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>

  <?php

  foreach ( $dropdowns as $slug => $title ) {

    if( isset( $referenceItems[$slug] ) && ! empty( $referenceItems[$slug] ) ) { ?>

  <div name="<?php echo $slug ?>" id="<?php echo $slug ?>" class="mt-3 mb-3 row">
    <div class="col">
      <div class="container">
        <h2><?php echo $title ?>
          <a data-toggle="tooltip" data-placement="top" title="Back to Top" class="ml-2" href="#theme-reference">
            <i class="fa fa-arrow-circle-up"></i>
          </a>
        </h2>

        <?php foreach ( $referenceItems[$slug] as $item ) { ?>

        <div id="shortcode-<?php echo $item['slug'] ?>" class="card" style="max-width:none; width:100%;">
          <div class="card-body">
            <h5 class="card-title">
              <?php echo $item['name'] ?>
              <a data-toggle="tooltip" data-placement="top" title="Back to Top" class="ml-2" href="#theme-reference">
                <i class="fa fa-arrow-circle-up"></i>
              </a>
            </h5>

            <?php 

            if( is_callable( $item['method'] ) )
              $item['method']();

            ?>

          </div>
        </div>

        <?php } ?>

      </div>
    </div>
  </div>

    <?php }

  } ?>

  <div name="sidebars" id="sidebars" class="row mt-3 mb-3">
    <div class="col">
      <div class="container">
        <h2>Sidebars
          <a data-toggle="tooltip" data-placement="top" title="Back to Top" class="ml-2" href="#theme-reference">
            <i class="fa fa-arrow-circle-up"></i>
          </a>
        </h2>
        <div class="alert alert-info" role="alert">
          <h5 class="alert-heading mt-1"><i class="fa fa-clock"></i> Coming Soon</h5>
        </div>
      </div>
    </div>
  </div>

  <div name="media" id="media" class="my-3 row">
    <div class="col">
      <div class="container">
        <h2>Media
          <a data-toggle="tooltip" data-placement="top" title="Back to Top" class="ml-2" href="#theme-reference">
            <i class="fa fa-arrow-circle-up"></i>
          </a>
        </h2>
        <div id="using-media" class="card" style="max-width:none; width:100%;">
          <div class="card-body">

            <?php Utility::getScopedTemplatePart(
              "template-parts/Bootstrap/admin/gallery",
              "edit"
            ) ?>

          </div>
        </div>
      </div>
    </div>
  </div>

</div>
