<?php

if( ! isset( $args ) )
  exit;

ob_start() ?>

    <h5 class="card-title">Editing Galleries</h5>

    <p class="card-text">
      In WordPress, Media is often referred to as "Media Attachments" because there is more context to it than just the image url(s), like alt text, titles, captions and descriptions.  Also because media is essentially "attached" to a post or page as an additional asset.  Referring to an "Attachment" includes all the meta details about it beyond just the image.  While there are many guides on how to manage Gallery Blocks online, they miss important steps on handling media attachment details. Follow this step-by-step guide to edit existing galleries, rearrange items in the gallery, and using best practices for handling media attachment details.  <i>The numbered steps annotate and correspond to the visual accompaniments below them</i>.
    </p>
    <ol>
      <li>Click/select the Gallery Block that you want to edit (you can click any image thumbnail in the preview to do this)</li>
      <li>Two buttons appear at the bottom of the block:  Upload / Media Library &mdash; click "Media Library"</li>
      <i>A modal shows, defaulting to the "Add to Gallery" tab on the left-side panel</i>
      <li>To add a media item to the gallery that has already been uploaded to the site, select it/them from the thumbnails shown here, then click the "Add to Gallery" button in the bottom right</li>
      <li>To upload a new media item to this gallery that does not exist on the site already, click the "Upload files" tab, drop in your files, and then click the "Add to Gallery" button in the bottom right</li>
      <li>Clicking the "Add to Gallery" button in either step 3 or 4 will move you over to the "Edit gallery" view on the left-side panel; you can also skip to this view by clicking the "Edit gallery" link on the left-side panel if you don't need to add new gallery items, and just want to change the order or details of existing media items in the gallery</li>
      <li>Click the "X" in the corner of a media item to remove it from the gallery</li>
      <li>Editing the caption in this view will update/sync it to the Attachment Details field (shown here)</li>
      <li>Always set an Alt Text value for every image in a gallery &mdash; it's integral for <a target="_new" href="https://www.w3.org/WAI/tutorials/images/decision-tree/" >accessibility standards</a></li>
    </ol>
    <figure style="max-width:600px">
      <img src="/wp-content/themes/parent.theme/assets/references/open-gallery-modal.png" alt="How to open the Edit Gallery modal" />
    </figure>
    <figure>
      <img src="/wp-content/themes/parent.theme/assets/references/add-to-gallery.png" alt="How to add new gallery media" />
    </figure>
    <figure>
      <img src="/wp-content/themes/parent.theme/assets/references/remove-edit-items.png" alt="How to remove or edit gallery media" />
      <figcaption class="alert alert-info"><i class="fa fa-info mr-3"></i> The "Description" field is not used in themes &mdash; make sure that any text you put in this field goes into the Caption instead to be seen on the website.</figcaption>
    </figure>

    <div class="alert alert-warning my-3">
      <h4 class="alert-heading">Warning: Static Captions</h4>
      <p>
        Editing the image captions only from the Gallery Block preview (directly in the post editor view, not in the Add/Edit Media modal) changes them only on THAT specific gallery, not everywhere that the media item is used across the site.  Themes will almost always reference the media attachment's caption, not the static caption on the gallery, so this text will almost never show as you expect.  It is not recommended to type captions only into this field; it is much better to set the caption at the Media Attachment level, where it will be used across the entire project (see step 7 above).
      </p>
      <p>
        However, when a gallery is created, any media items selected for the gallery that already had a caption set will show a static caption here.  The gallery's static caption is inherited from the Media Attachment's caption field (the one from step 7 above).
      </p>
    </div>
    <figure class="mt-3">
      <img src="/wp-content/themes/parent.theme/assets/references/static-captions.png" alt="Static captions are shown on the gallery preview in the block editor" />
    </figure>
    
    <div>
      <figure style="float:right; width:30%;">
        <img src="/wp-content/themes/parent.theme/assets/references/attachment-details.png" alt="Attachment detail fields" />
      </figure>
      <div class="alert alert-info my-3" style="float:left;width: 69%">
        <h4 class="alert-heading">Attachment Details</h4>
        <p>The best way to edit your media file details is using the <b>Attachment Details</b> fields, which show on the right-side panel when selecting a media item from the <a href="/wp-admin/upload.php">site's Media management view</a>, and from the Add/Edit Media modals (step 2 above).</p>
        <p>
          Some notes about Attachment Details fields:
        </p>
        <ul>
          <li><b>Alt text</b> is essential for accessibility and visually-impaired users; always fill in this field (see more below).</li>
          <li><b>Title</b> is optional, and would be used to show text when hovering an image, though it may not always be used in themes.</li>
          <li><b>Caption</b> is a subtext that will show under a photo when it is displayed on a page or in a gallery; use this to describe a photo's materials or dimensions, attribute credit, or cite a usage license.</li>
          <li><b>Description</b> is almost <b>never used</b> in theme development, and any text placed into this field will not show on the website.</li>
          <li><b>File URL</b> is a read-only field that is the public URL of the original size of the uploaded media file.  You can copy/paste this arond the web to share the photo.</li>
        </ul>
      </div>
      <div class="clearfix"></div>
    </div>

    <div class="alert alert-info my-3">
      <h4 class="alert-heading">Alternative Text</h4>
      When choosing media for a gallery, don't forget to write up an Alternative Text blurb to describe the image. The alt text can be set any time you have an image selected &mdash; whether in the "Add to Gallery" view or the "Edit Gallery" view. This is good for both search engine appearance, and for accessibility with sight-impaired users.  Here's where you can write up that Alternative Text:
    </div>
    <figure>
      <img class="mt-2" src="/wp-content/themes/parent.theme/assets/references/add-alt-text.png" alt="Adding alt text to gallery media" />
    </figure>

<?php echo ob_get_clean();
