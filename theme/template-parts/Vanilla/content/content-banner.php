<?php

if ( ! isset( $args ) )
  exit;

$html = "\n<div class='site-banner'>\n<div class='site-banner-inner'>";

if ( isset( $args['modal_toggle'] ) && ! empty( $args['modal_toggle'] ) ) {

    $shortcode = sprintf( "[modal style=' ' class='%s' label='%s']%s[/modal]", 'site-modal block box modal-target', $args['message'], $args['modal_content'] );
    $html .= do_shortcode($shortcode);

} else {

    $html .= $args['banner'];
    
}

$html .= "</div>\n</div>\n";
echo $html;
