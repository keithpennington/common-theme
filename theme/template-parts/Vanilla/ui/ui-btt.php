<?php
/**
 * The ui component for back-to-top element
 *
 * Template: UI BTT
 *
 * @package WordPress
 * @subpackage LOC Parent Theme
 */
?>

<div id="btt">
  <a tabindex="0" aria-label="Back to top" onclick="javascript:effects.scroll.animate(0)">
    <i class="fas fa-angle-up"></i>
    <i class="fas fa-angle-up"></i>
  </a>
</div>